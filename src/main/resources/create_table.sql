create table customer_table( id int not null AUTO_INCREMENT primary key, customer_id varchar(20),customer_name varchar(40));
create table user_table ( id int not null AUTO_INCREMENT primary key, username varchar(20), password varchar(20), level varchar(20));
create table universal_table ( id int not null AUTO_INCREMENT primary key, content varchar(50), type varchar(20));
create table stock_table ( id int not null AUTO_INCREMENT primary key, product int, quantity int, batch varchar(30), customer_id int );
create table product_import_record_table( id int not null AUTO_INCREMENT primary key, customer_id int, product int, storage varchar(50), batch_no varchar(40), quantity int,plate_no varchar(20), import_from varchar(30), transport varchar(50), import_time datetime, operator_id int, gmt_created datetime);
create table prouct_export_record_table( id int not null AUTO_INCREMENT primary key, customer_id int, product_id int, batch varchar(40), quantity int, operator_id int,  gmt_created datetime);
create table customer_relation_ship_table( id int not null AUTO_INCREMENT primary key, from_customer_id varchar(30), to_customer_id varchar(30), operation_type varchar(20), product varchar(30), batch_no varchar(40), quantity int, operator varchar(30), action_date datetime, gmt_created datetime);

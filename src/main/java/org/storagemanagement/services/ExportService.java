/**
 * 
 */
package org.storagemanagement.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.storagemanagement.DAO.ProductExportDao;
import org.storagemanagement.DO.ProductExportDO;
import org.storagemanagement.constant.StorageConstant;
import org.storagemanagement.model.ExportRecordQuery;
import org.storagemanagement.model.ExportRecordVO;

/**
 * @author Jason.zhang
 * 
 */
@Service("exportService")
public class ExportService {
	
	@Resource
	private ProductExportDao productExportDao;
	/**
	 * Add export record
	 * @throws ParseException 
	 */
	public void addExportRecord(ExportRecordVO exportRecord) throws ParseException {
		ProductExportDO productReduceRecordDO = new ProductExportDO();
		productReduceRecordDO.setBatchNo(exportRecord.getBatchNo());
		productReduceRecordDO.setOperator("Jason");
		productReduceRecordDO.setProduct(exportRecord.getProduct());
		productReduceRecordDO.setQuantity(exportRecord.getQuantity());
		productReduceRecordDO.setCustomerId(exportRecord.getCustomerId());
		productReduceRecordDO.setGmtCreated(new Date());
		productReduceRecordDO.setExportTime(StorageConstant.extJsDate.parse(exportRecord.getExportTime()));
		
		productExportDao.save(productReduceRecordDO);
	}

	/**
	 * Get export record for export table
	 * 
	 * @return
	 */
	public List<ExportRecordVO> getExportRecords(ExportRecordQuery exportQuery) {
		List<ExportRecordVO> exportRecords = new ArrayList<ExportRecordVO>();
		
		List<ProductExportDO> productReduceDOs = productExportDao.find(exportQuery);
		
		for (ProductExportDO productReduceDO:productReduceDOs) {
			ExportRecordVO exportRecordVO = new ExportRecordVO();
			exportRecordVO.setCustomerId(productReduceDO.getCustomerId());
			exportRecordVO.setCustomerName("Jason");
			exportRecordVO.setQuantity(productReduceDO.getQuantity());
			exportRecordVO.setBatchNo(productReduceDO.getBatchNo());
			exportRecordVO.setProduct(productReduceDO.getProduct());
			exportRecordVO.setOperator(productReduceDO.getOperator());
			exportRecordVO.setExportTime(StorageConstant.simpleDateFormat.format(productReduceDO.getExportTime()));
			exportRecords.add(exportRecordVO);
		}
		
		return exportRecords;
	}

	public void setProductExportDao(ProductExportDao productExportDao) {
		this.productExportDao = productExportDao;
	}
	
}
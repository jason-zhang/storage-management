package org.storagemanagement.services;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.storagemanagement.DAO.UserDao;
import org.storagemanagement.DO.UserDO;
import org.storagemanagement.model.UserQuery;
import org.storagemanagement.uti.UserTypeEnum;

/**
 * A sample user service to power the EXTJS application
 */
@Service("userService")
public class UserService {
	@Resource
	private UserDao userDao;
	
	public boolean isValidUser(String username, String password) {
		UserQuery query = new UserQuery();
		query.setUsername(username);
		query.setPassword(password);
		query.setLevel(UserTypeEnum.EMPLOYER);
		List<UserDO>users =  userDao.findUser(query);
		
		return !CollectionUtils.isEmpty(users) && users.size()==1 ? true : false;
	}

	public String getAutheticationKey(String userName) {
		return "12345";
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
}

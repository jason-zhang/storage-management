/**
 * 
 */
package org.storagemanagement.services;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.storagemanagement.DAO.CustomerRelationshipDao;
import org.storagemanagement.DAO.StockDao;
import org.storagemanagement.DO.CustomerRelationshipDO;
import org.storagemanagement.DO.StockDO;
import org.storagemanagement.constant.StorageConstant;
import org.storagemanagement.model.CustomerRelationshipQuery;
import org.storagemanagement.model.CustomerRelationshipVO;
import org.storagemanagement.model.StockQuery;

/**
 * @author Jason.zhang
 * 
 */
@Service("customerRelationshipService")
public class CustomerRelationshipService {
	
	@Resource
	private CustomerRelationshipDao customerRelationshipDao;
	
	@Resource
	private StockDao stockDao;
	
	public void borrowProduct(CustomerRelationshipVO customerRelationshipVO) throws Exception {
		CustomerRelationshipDO customerRelationDO =  new CustomerRelationshipDO();
		
		customerRelationDO.setProductFrom(customerRelationshipVO.getProductFrom());
		customerRelationDO.setBatchNo(customerRelationshipVO.getBatchNo());
		customerRelationDO.setProductTo(customerRelationshipVO.getProductTo());
		customerRelationDO.setActionDate(StorageConstant.simpleDateFormat.parse(customerRelationshipVO.getActionTime()));
		customerRelationDO.setGmtCreate(new Date());
		customerRelationDO.setOperator("Jason");
		customerRelationDO.setProduct(customerRelationshipVO.getProduct());
		customerRelationDO.setQuantity(customerRelationshipVO.getQuantity());
		customerRelationDO.setOperationType(customerRelationshipVO.getActionType());
		
		/* Minus stock from who borrow */
		List<StockDO> stocksFrom = this.stockDao.findStock(new StockQuery(customerRelationshipVO.getProductFrom()));
		boolean productExistInOwner = false;
		for(StockDO stockDO : stocksFrom ){
			if( stockDO.getCustomerId().equals(customerRelationDO.getProductFrom()) &&
				stockDO.getBatchNo().equals(customerRelationDO.getBatchNo()	)){
				if(stockDO.getQuantity() < customerRelationDO.getQuantity()){
					throw new Exception("not enough product");
				}
				stockDO.setQuantity(stockDO.getQuantity() - customerRelationDO.getQuantity());
				this.stockDao.update(stockDO);
				productExistInOwner  = true;
				break;
			}
		}
		
		if(!productExistInOwner){
			throw new Exception("not product found");
		}

		/* Add stock to who borrow*/
		/* if product exist, add product to existing record, else create new record */
		List<StockDO> stocksTo = this.stockDao.findStock(new StockQuery(customerRelationshipVO.getProductTo()));
		boolean productExistInBorrower = false;
		for(StockDO stockDO : stocksTo ){
			if( stockDO.getCustomerId().equals(customerRelationDO.getProductTo()) &&
				stockDO.getBatchNo().equals(customerRelationDO.getBatchNo()	)){
				stockDO.setQuantity(stockDO.getQuantity() + customerRelationDO.getQuantity());
				this.stockDao.update(stockDO);
				productExistInBorrower  = true;
				break;
			}
		}
		
		if(!productExistInBorrower){
			StockDO stockDO = new StockDO();
			stockDO.setBatchNo(customerRelationDO.getBatchNo());
			stockDO.setCustomerId(customerRelationDO.getProductTo());
			stockDO.setProduct(customerRelationDO.getProduct());
			stockDO.setQuantity(customerRelationDO.getQuantity());
			stockDO.setGmtModified(new Date());
			this.stockDao.save(stockDO);
		}
		
		this.customerRelationshipDao.add(customerRelationDO);
	}

	public void returnProduct(CustomerRelationshipVO customerRelationshipVO) throws Exception {
		this.borrowProduct(customerRelationshipVO);
	}

	public void buySellProduct(CustomerRelationshipVO customerRelationshipVO) throws Exception {
		this.borrowProduct(customerRelationshipVO);
	}


	public List<CustomerRelationshipDO> getCustomerRelationShip(CustomerRelationshipQuery query){
		return customerRelationshipDao.find(query);
	}

	public void setCustomerRelationshipDao(CustomerRelationshipDao customerRelationshipDao) {
		this.customerRelationshipDao = customerRelationshipDao;
	}

	public void setStockDao(StockDao stockDao) {
		this.stockDao = stockDao;
	}
	
}

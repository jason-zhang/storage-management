/**
 * 
 */
package org.storagemanagement.services;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.storagemanagement.DAO.ProductImportDao;
import org.storagemanagement.DO.ProductImportDO;
import org.storagemanagement.constant.StorageConstant;
import org.storagemanagement.model.ImportRecordQuery;
import org.storagemanagement.model.ImportRecordVO;

/**
 * @author Jason.zhang
 */
@Service("importService")
public class ImportService {
	
	@Resource
	private ProductImportDao productImportDao;
	/**
	 * retrieve import record for import table
	 * 
	 * @return
	 */
	public List<ImportRecordVO> getImportRecords(ImportRecordQuery importQuery) {
		List<ImportRecordVO> importRecords = new ArrayList<ImportRecordVO>();
		
		List<ProductImportDO> products = productImportDao.find(importQuery);
		
		for(ProductImportDO productAddRecord : products){
			ImportRecordVO importRecordVO = new ImportRecordVO();
			importRecordVO.setImportFrom(productAddRecord.getImportFrom());
			importRecordVO.setBatchNo(productAddRecord.getBatchNo());
			importRecordVO.setCustomerId(productAddRecord.getCustomerId());
			importRecordVO.setImportTime(StorageConstant.simpleDateFormat.format(productAddRecord.getImportTime()));
			importRecordVO.setPlateNo(productAddRecord.getPlateNo());
			importRecordVO.setProduct(productAddRecord.getProduct());
			importRecordVO.setQuantity(productAddRecord.getQuantity());
			importRecordVO.setStorage(productAddRecord.getStorage());
			importRecordVO.setTransport(productAddRecord.getTransport());
			
			importRecords.add(importRecordVO);
		}
		return importRecords;
	}

	/**
	 * add import record
	 * 
	 * @param importRecordList
	 * @throws ParseException 
	 */
	public void addImportRecord(ImportRecordVO importRecordVO, String operator) throws ParseException {
		ProductImportDO importRecordDO = toRecordDO(importRecordVO, operator);
		productImportDao.save(importRecordDO);
	}

	public void setProductImportDao(ProductImportDao productAddDao) {
		this.productImportDao = productAddDao;
	}
	
	private ProductImportDO toRecordDO(ImportRecordVO importRecordVO, String operator) throws ParseException{
		ProductImportDO importRecordDO = new ProductImportDO();
		importRecordDO.setImportFrom(importRecordVO.getImportFrom());
		importRecordDO.setBatchNo(importRecordVO.getBatchNo());
		importRecordDO.setCustomerId(importRecordVO.getCustomerId());
		importRecordDO.setGmtCreated(new Date());
		importRecordDO.setImportTime(StorageConstant.simpleDateFormat.parse(importRecordVO.getImportTime()));
		importRecordDO.setPlateNo(importRecordVO.getPlateNo());
		importRecordDO.setProduct(importRecordVO.getProduct());
		importRecordDO.setQuantity(importRecordVO.getQuantity());
		importRecordDO.setStorage(importRecordVO.getStorage());
		importRecordDO.setTransport(importRecordVO.getTransport());
		importRecordDO.setOperator(operator);
		return importRecordDO;
	}
}

/**
 * 
 */
package org.storagemanagement.services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.storagemanagement.DAO.UniversalDao;
import org.storagemanagement.DO.UniversalDO;

/**
 * @author jason.zhang
 *
 */
@Service("universalService")
public class UniversalService {
	@Resource
	private UniversalDao universalDao;
	
	public List<String> getContents(String type){
		List<UniversalDO>universalDOs = universalDao.findByType(type);
		List<String> contents = new ArrayList<String>();
		for(UniversalDO universalDO:universalDOs){
			contents.add(universalDO.getContent());
		}
		return contents;
	}
	
	public void setUniversalDao(UniversalDao universalDao) {
		this.universalDao = universalDao;
	}
}

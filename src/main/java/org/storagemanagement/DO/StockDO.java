/**
 * 
 */
package org.storagemanagement.DO;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Jason.zhang
 * 
 */
@Entity
@Table(name = "stock_table")
public class StockDO implements Serializable {

	private static final long serialVersionUID = 96262146L;

	@Id
	@Column(name = "id")
	@GeneratedValue
	private int id;

	@Column(name = "product")
	private String product;

	@Column(name = "quantity")
	private int quantity;

	@Column(name = "batch_no")
	private String batchNo;

	@Column(name = "customer_id")
	private String customerId;

	@Column(name = "gmt_modified")
	private Date gmtModified;
	
	public StockDO() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerId() {
		return this.customerId;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public Date getGmtModified() {
		return this.gmtModified;
	}

	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}

}

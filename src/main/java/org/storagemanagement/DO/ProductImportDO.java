/**
 * 
 */
package org.storagemanagement.DO;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Jason.zhang
 * 
 */
@Entity
@Table(name = "product_import_table")
public class ProductImportDO implements Serializable {

	private static final long serialVersionUID = 528523632L;

	@Id
	@Column(name = "id")
	@GeneratedValue
	private int id;

	@Column(name = "customer_id")
	private String customerId;
	
	@Column(name = "product")
	private String product;

	@Column(name = "storage")
	private String storage;

	@Column(name = "batch_no")
	private String batchNo;

	@Column(name = "quantity")
	private int quantity;

	@Column(name = "import_from")
	private String importFrom;

	@Column(name = "transport")
	private String transport;
	
	@Column(name = "import_time")
	private Date importTime;
	
	@Column(name = "plate_no")
	private String plateNo;
	
	@Column(name = "operator")
	private String operator;

	@Column(name = "gmt_created")
	private Date gmtCreated;

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getStorage() {
		return this.storage;
	}

	public void setStorage(String storage) {
		this.storage = storage;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getImportFrom() {
		return this.importFrom;
	}

	public void setImportFrom(String importFrom) {
		this.importFrom = importFrom;
	}

	public String getTransport() {
		return this.transport;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}

	public Date getImportTime() {
		return this.importTime;
	}

	public void setImportTime(Date importTime) {
		this.importTime = importTime;
	}

	public String getPlateNo() {
		return this.plateNo;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Date getGmtCreated() {
		return this.gmtCreated;
	}

	public void setGmtCreated(Date gmtCreated) {
		this.gmtCreated = gmtCreated;
	}
	
}

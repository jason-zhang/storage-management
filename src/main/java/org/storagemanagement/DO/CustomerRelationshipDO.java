/**
 * 
 */
package org.storagemanagement.DO;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author jason.zhang
 *
 */
@Entity
@Table(name = "customer_relation_ship_table")
public class CustomerRelationshipDO {
	@Id
	@Column(name = "id")
	@GeneratedValue
	private Integer id;
	
	@Column(name = "operation_type")
	private String operationType;
	
	@Column(name = "from_customer_id")
	private String productFrom;
	
	@Column(name = "to_customer_id")
	private String productTo;
	
	@Column(name = "product")
	private String product;
	
	@Column(name = "quantity")
	private int quantity;
	
	@Column(name = "batch_no")
	private String batchNo;
	
	@Column(name = "operator")
	private String operator;
	
	@Column(name = "action_date")
	private Date actionDate;
	
	@Column(name = "gmt_created")
	private Date gmtCreate;
	
	public Integer getId() {
		return this.id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getOperationType() {
		return this.operationType;
	}
	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}
	public String getProductFrom() {
		return this.productFrom;
	}
	public void setProductFrom(String productFrom) {
		this.productFrom = productFrom;
	}
	public String getProductTo() {
		return this.productTo;
	}
	public void setProductTo(String productTo) {
		this.productTo = productTo;
	}
	public String getProduct() {
		return this.product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public int getQuantity() {
		return this.quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getBatchNo() {
		return this.batchNo;
	}
	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	public String getOperator() {
		return this.operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public Date getActionDate() {
		return this.actionDate;
	}
	public void setActionDate(Date actionDate) {
		this.actionDate = actionDate;
	}
	public Date getGmtCreate() {
		return this.gmtCreate;
	}
	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
}

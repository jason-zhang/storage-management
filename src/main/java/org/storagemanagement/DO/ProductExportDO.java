/**
 * 
 */
package org.storagemanagement.DO;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Jason.zhang
 * 
 */
@Entity
@Table(name = "product_export_table")
public class ProductExportDO implements Serializable {

	private static final long serialVersionUID = 123123435262465L;

	@Id
	@GeneratedValue
	private int id;

	@Column(name = "customer_id")
	private String customerId;

	@Column(name = "product")
	private String product;

	@Column(name = "batch_no")
	private String batchNo;

	@Column(name = "quantity")
	private int quantity;

	@Column(name = "operator")
	private String operator;

	@Column(name = "gmt_created")
	private Date gmtCreated;

	@Column(name = "export_time")
	private Date exportTime;

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getOperator() {
		return this.operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Date getGmtCreated() {
		return this.gmtCreated;
	}

	public void setGmtCreated(Date gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Date getExportTime() {
		return this.exportTime;
	}

	public void setExportTime(Date exportTime) {
		this.exportTime = exportTime;
	}
}

/**
 * 
 */
package org.storagemanagement.DO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Jason.zhang
 * 
 */
@Entity
@Table(name = "universal_table")
public class UniversalDO implements Serializable {

	private static final long serialVersionUID = 343214123565L;

	@Id
	@GeneratedValue
	private int id;

	@Column(name = "content")
	private String content;
	
	@Column(name = "type")
	private String type;

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}

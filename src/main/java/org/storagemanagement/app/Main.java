/**
 * 
 */
package org.storagemanagement.app;

/**
 * @author Jason.zhanga
 *
 */
import java.util.List;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.storagemanagement.DO.StockDO;
import org.storagemanagement.model.ImportRecordQuery;
import org.storagemanagement.model.ImportRecordVO;
import org.storagemanagement.services.ImportService;
import org.storagemanagement.services.StockService;

public class Main {

	public static void main(String[] args) {
		testImportkDAO();
	}
	
	public static void testImportkDAO(){
		
		ApplicationContext appContext = new ClassPathXmlApplicationContext("BeanLocations.xml");
		
		ImportService importService = (ImportService) appContext.getBean("importService");
		ImportRecordQuery importQuery = new ImportRecordQuery();
		importQuery.setCustomerId("1");
		importQuery.setProduct("apt1");
		importQuery.setBatchNo("3016");
		importQuery.setImportDateFrom("2013-09-23");
		importQuery.setImportDateTo("2013-09-24");
		List<ImportRecordVO> importVOs = importService.getImportRecords(importQuery);

		System.out.println(ReflectionToStringBuilder.toString(importVOs));
	}
	
	public static void testStockDAO(){
		ApplicationContext appContext = new ClassPathXmlApplicationContext("BeanLocations.xml");
		StockDO stockDO = new StockDO();
		stockDO.setId(1);
		stockDO.setCustomerId("");
		stockDO.setProduct("");
		stockDO.setQuantity(20);
		stockDO.setBatchNo("5555");

		StockService stockService = (StockService) appContext.getBean("stockService");
		stockService.updateStock(stockDO);

		stockService.getStocksByCustomer("1");
	}
}
/**
 * 
 */
package org.storagemanagement.constant;

import java.text.SimpleDateFormat;

/**
 * @author Jason.zhang
 * 
 */
public class StorageConstant {
	public final static String LOGIN_VO = "loginVO";
	public final static SimpleDateFormat dateFormatWithSeconds =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public final static SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd");
	public final static SimpleDateFormat extJsDate =  new SimpleDateFormat("MM/dd/yyyy");
}

/**
 * 
 */
package org.storagemanagement.model;

import org.storagemanagement.uti.UserTypeEnum;

/**
 * @author chengsen.zhangcs
 * 
 */
public class UserQuery {
	private String username;
	private String password;
	private UserTypeEnum level;
	
	public String getUsername() {
		return this.username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public UserTypeEnum getLevel() {
		return this.level;
	}
	public void setLevel(UserTypeEnum level) {
		this.level = level;
	}
}

/**
 * 
 */
package org.storagemanagement.model;


/**
 * @author Jason.zhang
 * 
 */
public class ImportRecordVO {
	private String customerName;
	private String customerId;
	private String product;
	private String batchNo;
	private String storage;
	private int quantity;
	private String plateNo;
	private String transport;
	private String importFrom;
	private String importTime;

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getStorage() {
		return this.storage;
	}

	public void setStorage(String storage) {
		this.storage = storage;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getImportFrom() {
		return this.importFrom;
	}

	public void setImportFrom(String importFrom) {
		this.importFrom = importFrom;
	}

	public String getPlateNo() {
		return this.plateNo;
	}

	public String getImportTime() {
		return this.importTime;
	}

	public void setImportTime(String importTime) {
		this.importTime = importTime;
	}

	public void setPlateNo(String plateNo) {
		this.plateNo = plateNo;
	}

	public String getTransport() {
		return this.transport;
	}

	public void setTransport(String transport) {
		this.transport = transport;
	}

}

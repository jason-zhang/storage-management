/**
 * 
 */
package org.storagemanagement.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author chengsen.zhangcs
 * 
 */
public class QuantityWithBatchAndDate {
	private int quantity;
	private String batchNo;
	private Date importExportDate;
	private boolean isImport;

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getImportExportDate() {
		return new SimpleDateFormat("yyyy-MM-dd").format(importExportDate);
	}

	public void setImportExportDate(Date importExportDate) {
		this.importExportDate = importExportDate;
	}

	public boolean isImport() {
		return this.isImport;
	}

	public void setImport(boolean isImport) {
		this.isImport = isImport;
	}
}

/**
 * 
 */
package org.storagemanagement.model;

import java.util.List;

/**
 * @author chengsen
 * 
 */
public class ImportRecordVOFace {
	List<ImportRecordVO> records;

	public List<ImportRecordVO> getRecords() {
		return this.records;
	}

	public void setRecords(List<ImportRecordVO> records) {
		this.records = records;
	}

}

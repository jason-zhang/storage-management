/**
 * 
 */
package org.storagemanagement.model;

/**
 * @author chengsen
 * 
 */
public class BatchAndQuantity {
	private int quantity;
	private String batchNo;

	public BatchAndQuantity() {
	}

	public BatchAndQuantity(String batchNo, int quantity) {
		this.batchNo = batchNo;
		this.quantity = quantity;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

}

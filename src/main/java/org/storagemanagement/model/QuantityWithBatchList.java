/**
 * 
 */
package org.storagemanagement.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jason.zhang
 * 
 */
public class QuantityWithBatchList {
	private int quantity;
	private List<BatchAndQuantity> batches;

	public int getQuantity() {
		return this.quantity;
	}

	public List<BatchAndQuantity> getBatches() {
		return this.batches;
	}


	public void addBatchAndQuanlity(String batchNo, int quantity) {
		if (this.batches == null) {
			this.batches = new ArrayList<BatchAndQuantity>();
		}
		
		this.batches.add(new BatchAndQuantity(batchNo, quantity));
		this.quantity = this.quantity + quantity;
	}

}

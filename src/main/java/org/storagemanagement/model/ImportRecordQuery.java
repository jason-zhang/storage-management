/**
 * 
 */
package org.storagemanagement.model;



/**
 * @author Jason.zhang
 * 
 */
public class ImportRecordQuery {

	private String customerId;
	private String product;
	private String batchNo;
	private String importDateFrom;
	private String importDateTo;

	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getImportDateFrom() {
		return this.importDateFrom;
	}

	public void setImportDateFrom(String importDateFrom) {
		this.importDateFrom = importDateFrom;
	}

	public String getImportDateTo() {
		return this.importDateTo;
	}

	public void setImportDateTo(String importDateTo) {
		this.importDateTo = importDateTo;
	}
}

/**
 * 
 */
package org.storagemanagement.model;

import java.util.List;

/**
 * @author jason.zhang
 * 
 */
public class StockVOWithExportImportRecord {
	private String customerId;
	private String product;
	private List<QuantityWithBatchAndDate> quantityWithRecordList;

	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public List<QuantityWithBatchAndDate> getQuantityWithRecordList() {
		return this.quantityWithRecordList;
	}

	public void setQuantityWithRecordList(List<QuantityWithBatchAndDate> quantityWithRecordList) {
		this.quantityWithRecordList = quantityWithRecordList;
	}
}

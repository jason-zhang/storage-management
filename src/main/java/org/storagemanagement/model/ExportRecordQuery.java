/**
 * 
 */
package org.storagemanagement.model;


/**
 * @author Jason.zhang
 * 
 */
public class ExportRecordQuery {

	private String customerId;
	private String exportDateFrom;
	private String exportDateTo;
	private String product;
	private String batchNo;

	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getExportDateFrom() {
		return this.exportDateFrom;
	}

	public void setExportDateFrom(String exportDateFrom) {
		this.exportDateFrom = exportDateFrom;
	}

	public String getExportDateTo() {
		return this.exportDateTo;
	}

	public void setExportDateTo(String exportDateTo) {
		this.exportDateTo = exportDateTo;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}
	
}

/**
 * 
 */
package org.storagemanagement.model;

import org.storagemanagement.DO.StockDO;

/**
 * @author Jason.zhang
 * 
 */
public class StockVOWithBatchList {

	private String customerName;
	private String customerId;
	private String product;
	private QuantityWithBatchList quantityWithBatchList;

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public QuantityWithBatchList getQuantityWithBatchList() {
		return this.quantityWithBatchList;
	}

	public void setQuantityWithBatchList(QuantityWithBatchList quantityWithBatchList) {
		this.quantityWithBatchList = quantityWithBatchList;
	}
	
	public void addStockToList(StockDO stockDO){
		if (this.quantityWithBatchList == null){
			this.quantityWithBatchList = new QuantityWithBatchList();
		}
		this.quantityWithBatchList.addBatchAndQuanlity(stockDO.getBatchNo(), stockDO.getQuantity());
	}

}

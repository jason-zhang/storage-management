/**
 * 
 */
package org.storagemanagement.model;


/**
 * @author jason.zhang
 *
 */
public class CustomerRelationshipQuery {
	private String productFrom;
	private String productTo;
	private String product;
	private String dateFrom;
	private String dateTo;
	
	public String getProductFrom() {
		return this.productFrom;
	}
	public void setProductFrom(String productFrom) {
		this.productFrom = productFrom;
	}
	public String getProductTo() {
		return this.productTo;
	}
	public void setProductTo(String productTo) {
		this.productTo = productTo;
	}
	public String getProduct() {
		return this.product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public String getDateFrom() {
		return this.dateFrom;
	}
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	public String getDateTo() {
		return this.dateTo;
	}
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

}

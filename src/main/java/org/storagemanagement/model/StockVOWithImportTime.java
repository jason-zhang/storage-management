/**
 * 
 */
package org.storagemanagement.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Jason.zhang
 * 
 */
public class StockVOWithImportTime extends StockVO {

	private Date importTime;

	public Date getImportTime() {
		return this.importTime;
	}
	
	public void setImportTime(Date importTime) {
		this.importTime = importTime;
	}

	public void setImportTime(String importTime) {
		try {
			this.importTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(importTime);
		} catch (ParseException e) {
			System.out.println("parse date failed");
			e.printStackTrace();
		}
	}
}

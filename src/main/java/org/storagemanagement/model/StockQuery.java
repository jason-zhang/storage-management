/**
 * 
 */
package org.storagemanagement.model;

/**
 * @author chengsen.zhangcs
 * 
 */
public class StockQuery {
	private String customerId;
	private String product;
	private String batchNo;
	private String importDateFrom;
	private String importDateTo;

	public StockQuery(){}
	
	public StockQuery(String customerId){
		this.customerId = customerId;
	}
	public StockQuery(String customerId, String product){
		this.customerId = customerId;
		this.product = product;
	}
	
	
	public StockQuery(String customerId, String product, String batchNo){
		this.customerId = customerId;
		this.product = product;
		this.batchNo = batchNo;
	}
	
	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String productType) {
		this.product = productType;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getImportDateFrom() {
		return this.importDateFrom;
	}

	public void setImportDateFrom(String importDateFrom) {
		this.importDateFrom = importDateFrom;
	}

	public String getImportDateTo() {
		return this.importDateTo;
	}

	public void setImportDateTo(String importDateTo) {
		this.importDateTo = importDateTo;
	}
}

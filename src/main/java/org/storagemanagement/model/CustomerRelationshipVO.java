/**
 * 
 */
package org.storagemanagement.model;

/**
 * @author Jason.zhang
 * 
 */
public class CustomerRelationshipVO {
	private String productFrom;
	private String productTo;
	private String batchNo;
	private String product;
	private int quantity;
	private String actionTime;
	private String actionType;

	public String getProductFrom() {
		return this.productFrom;
	}

	public void setProductFrom(String productFrom) {
		this.productFrom = productFrom;
	}

	public String getProductTo() {
		return this.productTo;
	}

	public void setProductTo(String productTo) {
		this.productTo = productTo;
	}

	public String getBatchNo() {
		return this.batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getProduct() {
		return this.product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getActionTime() {
		return this.actionTime;
	}

	public void setActionTime(String actionTime) {
		this.actionTime = actionTime;
	}

	public String getActionType() {
		return this.actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

}

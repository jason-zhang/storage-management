/**
 * 
 */
package org.storagemanagement.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.storagemanagement.constant.StorageConstant;
import org.storagemanagement.model.LoginVO;
import org.storagemanagement.services.UniversalService;
import org.storagemanagement.uti.LoginUtils;

/**
 * @author Jason.zhang
 * 
 */
@Controller
@RequestMapping("/universal")
@SessionAttributes(value = StorageConstant.LOGIN_VO, types = LoginVO.class)
public class UniversalController {
	@Resource
	private UniversalService universalService;

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping("/getContent")
	public @ResponseBody
	List<String> getContent(@RequestParam("type") String type, @ModelAttribute(StorageConstant.LOGIN_VO) LoginVO loginVO) {
		if(!LoginUtils.isAuthed(loginVO)){
			throw new RuntimeException("Please login first");
		}
		return universalService.getContents(type);
	}

	public void setUniversalService(UniversalService universalService) {
		this.universalService = universalService;
	}
}

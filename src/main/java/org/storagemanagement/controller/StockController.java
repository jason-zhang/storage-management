/**
 * 
 */
package org.storagemanagement.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.storagemanagement.constant.StorageConstant;
import org.storagemanagement.model.LoginVO;
import org.storagemanagement.model.StockQuery;
import org.storagemanagement.model.StockVO;
import org.storagemanagement.model.StockVOWithBatchList;
import org.storagemanagement.model.StockVOWithExportImportRecord;
import org.storagemanagement.services.StockService;
import org.storagemanagement.uti.LoginUtils;

/**
 * @author Jason.zhang
 * 
 */
@Controller
@RequestMapping("/stock")
@SessionAttributes(value = StorageConstant.LOGIN_VO, types = LoginVO.class)
public class StockController {
	@Resource
	private StockService stockService;

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping("/get_products")
	public @ResponseBody
	List<StockVO> getRecords(@RequestParam("customerId") String customerId , @ModelAttribute(StorageConstant.LOGIN_VO) LoginVO loginVO) {
		if(!LoginUtils.isAuthed(loginVO)){
			throw new RuntimeException("Please login first");
		}
		List<StockVO> stocks = stockService.getStocksByCustomer(customerId);
		return stocks;
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping("/get_records_grouped_by_product")
	public @ResponseBody
	List<StockVOWithBatchList> getRecordsGroupedByProduct(@RequestParam("customerId") String customerId , @ModelAttribute(StorageConstant.LOGIN_VO) LoginVO loginVO) {
		if(!LoginUtils.isAuthed(loginVO)){
			throw new RuntimeException("Please login first");
		}
		List<StockVOWithBatchList> stocks = stockService.getStocksGroupedByProduct(customerId);
		return stocks;
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping("/get_records_with_import_export_record")
	public @ResponseBody
	List<StockVOWithExportImportRecord> getStockWithImportExportRecord(@ModelAttribute StockQuery stockQuery , @ModelAttribute(StorageConstant.LOGIN_VO) LoginVO loginVO) {
		if(!LoginUtils.isAuthed(loginVO)){
			throw new RuntimeException("Please login first");
		}
		List<StockVOWithExportImportRecord> stocks = stockService.getStocksWithExportImportRecord(stockQuery);
		return stocks;
	}

	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}

}

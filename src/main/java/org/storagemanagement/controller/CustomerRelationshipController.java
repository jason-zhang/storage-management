/**
 * 
 */
package org.storagemanagement.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.storagemanagement.DO.CustomerRelationshipDO;
import org.storagemanagement.constant.StorageConstant;
import org.storagemanagement.model.CustomerRelationshipQuery;
import org.storagemanagement.model.CustomerRelationshipVO;
import org.storagemanagement.model.LoginVO;
import org.storagemanagement.services.CustomerRelationshipService;
import org.storagemanagement.uti.LoginUtils;
import org.storagemanagement.uti.OperationTypeEnum;

/**
 * @author Jason.zhang
 * 
 */
@Controller
@RequestMapping("/customer_relationship")
@SessionAttributes(value = StorageConstant.LOGIN_VO, types = LoginVO.class)
public class CustomerRelationshipController {
	@Resource
	private CustomerRelationshipService customerRelationshipService;

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/borrow/add_borrow_record")
	public @ResponseBody
	void borrow( @RequestBody CustomerRelationshipVO customerRelationshipVO, @ModelAttribute(StorageConstant.LOGIN_VO) LoginVO loginVO ) throws Exception {
		if(!LoginUtils.isAuthed(loginVO)){
			throw new RuntimeException("Please login first");
		}
		customerRelationshipVO.setActionType(OperationTypeEnum.BORROW.getType());
		customerRelationshipService.borrowProduct(customerRelationshipVO);
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping("/return/add_return_record")
	public @ResponseBody
	boolean returnProduct(@RequestBody CustomerRelationshipVO customerRelationshipVO, @ModelAttribute(StorageConstant.LOGIN_VO) LoginVO loginVO) {
		if(!LoginUtils.isAuthed(loginVO)){
			throw new RuntimeException("Please login first");
		}
		
		boolean flag = false;
		try {
			customerRelationshipVO.setActionType(OperationTypeEnum.RETURN.getType());
			customerRelationshipService.returnProduct(customerRelationshipVO);
			flag = true;
		} catch (Exception ex) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/buysell/add_buysell_record")
	public @ResponseBody
	void buySellProduct( @RequestBody CustomerRelationshipVO customerRelationshipVO, @ModelAttribute(StorageConstant.LOGIN_VO) LoginVO loginVO) throws Exception {
		if(!LoginUtils.isAuthed(loginVO)){
			throw new RuntimeException("Please login first");
		}
		customerRelationshipVO.setActionType(OperationTypeEnum.BUY_SELL.getType());
		customerRelationshipService.buySellProduct(customerRelationshipVO);
	}
	
	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping("/get_records")
	public @ResponseBody
	List<CustomerRelationshipVO> getRecords(@ModelAttribute CustomerRelationshipQuery query, @ModelAttribute(StorageConstant.LOGIN_VO) LoginVO loginVO) {
		if(!LoginUtils.isAuthed(loginVO)){
			throw new RuntimeException("Please login first");
		}

		List<CustomerRelationshipDO> relationRecords = customerRelationshipService.getCustomerRelationShip(query);
		List<CustomerRelationshipVO> relationshipVO = convertToVO(relationRecords);
		return relationshipVO;
	}

	/**
	 * 
	 * @param customerRelationshipService
	 */
	private List<CustomerRelationshipVO> convertToVO(List<CustomerRelationshipDO> relationRecords){
		List<CustomerRelationshipVO> customerRelationshipVOs = new ArrayList<CustomerRelationshipVO>();
		for(CustomerRelationshipDO relationshipDO : relationRecords){
			CustomerRelationshipVO customerRelationshipVO = new CustomerRelationshipVO();
			customerRelationshipVO.setActionTime(StorageConstant.simpleDateFormat.format(relationshipDO.getActionDate()));
			customerRelationshipVO.setBatchNo(relationshipDO.getBatchNo());
			customerRelationshipVO.setProduct(relationshipDO.getProduct());
			customerRelationshipVO.setProductFrom(relationshipDO.getProductFrom());
			customerRelationshipVO.setProductTo(relationshipDO.getProductTo());
			customerRelationshipVO.setQuantity(relationshipDO.getQuantity());
			customerRelationshipVO.setActionType(relationshipDO.getOperationType());
			
			customerRelationshipVOs.add(customerRelationshipVO);
		}
		return customerRelationshipVOs;
	}
	
	public void setCustomerRelationshipService(CustomerRelationshipService customerRelationshipService) {
		this.customerRelationshipService = customerRelationshipService;
	}

}

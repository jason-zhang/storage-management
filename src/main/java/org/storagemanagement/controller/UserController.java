/**
 * 
 */
package org.storagemanagement.controller;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.storagemanagement.constant.StorageConstant;
import org.storagemanagement.model.LoginVO;
import org.storagemanagement.services.UserService;

/**
 * @author Jason.zhang
 * 
 */
@Controller
@RequestMapping("/user")
@Scope("session")
@SessionAttributes({StorageConstant.LOGIN_VO})
public class UserController {
	@Resource
	private UserService userService;

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping("/authenticateUser")
	public @ResponseBody
	String authenticateUser(@RequestParam("username") String username, @RequestParam("password") String password, ModelMap model) {
		boolean canLogin = userService.isValidUser(username, password);
		LoginVO loginVO = new LoginVO();
		if (!canLogin) {
			return "{'success': false }";
		}
		loginVO.setLogin(canLogin);	
		model.addAttribute(StorageConstant.LOGIN_VO, loginVO);
		
		return "{'success': true }";
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}

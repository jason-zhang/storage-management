/**
 * 
 */
package org.storagemanagement.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.storagemanagement.DO.StockDO;
import org.storagemanagement.constant.StorageConstant;
import org.storagemanagement.model.ExportRecordQuery;
import org.storagemanagement.model.ExportRecordVO;
import org.storagemanagement.model.LoginVO;
import org.storagemanagement.model.StockVOWithImportTime;
import org.storagemanagement.services.ExportService;
import org.storagemanagement.services.StockService;
import org.storagemanagement.uti.LoginUtils;

/**
 * @author Jason.zhang
 * 
 */
@Controller
@RequestMapping("/export")
@SessionAttributes(value = StorageConstant.LOGIN_VO, types = LoginVO.class)
public class ExportController {
	@Resource
	private ExportService exportService;
	@Resource
	private StockService stockService;

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping("/get_records")
	public @ResponseBody
	List<ExportRecordVO> getRecords(@ModelAttribute ExportRecordQuery exportQuery, @ModelAttribute(StorageConstant.LOGIN_VO) LoginVO loginVO) {
		if(!LoginUtils.isAuthed(loginVO)){
			throw new RuntimeException("Please login first");
		}

		List<ExportRecordVO> exportRecords = exportService.getExportRecords(exportQuery);
		return exportRecords;
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping("/add_record")
	public @ResponseBody
	boolean addRecords(@RequestBody ExportRecordVO exportRecord , @ModelAttribute(StorageConstant.LOGIN_VO) LoginVO loginVO) {
		if(!LoginUtils.isAuthed(loginVO)){
			throw new RuntimeException("Please login first");
		}
		boolean flag = false;
		try {
			exportService.addExportRecord(exportRecord);
			
			StockDO stockDO = new StockDO();
			stockDO.setBatchNo(exportRecord.getBatchNo());
			stockDO.setCustomerId(exportRecord.getCustomerId());
			stockDO.setProduct(exportRecord.getProduct());
			stockDO.setQuantity(exportRecord.getQuantity());
			stockService.reduceStock(stockDO);
			flag = true;
		} catch (Exception ex) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 
	 * @param username
	 * @param password
	 * @return
	 */
	@RequestMapping("/get_existing_products")
	public @ResponseBody
	List<StockVOWithImportTime> getExistingProducts(@RequestParam("customerId") String customerId,@RequestParam("product") String product , @ModelAttribute(StorageConstant.LOGIN_VO) LoginVO loginVO) {
		if(!LoginUtils.isAuthed(loginVO)){
			throw new RuntimeException("Please login first");
		}

		List<StockVOWithImportTime> stockVOs = stockService.getStockWithImportTime(customerId,product);
		return stockVOs;
	}

	public void setStockService(StockService stockService) {
		this.stockService = stockService;
	}

	public void setExportService(ExportService exportService) {
		this.exportService = exportService;
	}

}

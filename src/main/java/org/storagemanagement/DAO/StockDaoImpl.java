/**
 * 
 */
package org.storagemanagement.DAO;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.springframework.stereotype.Repository;
import org.storagemanagement.DO.StockDO;
import org.storagemanagement.model.StockQuery;
import org.storagemanagement.uti.CustomHibernateDaoSupport;

/**
 * @author chengsen
 * 
 */
@Repository("stockDao")
public class StockDaoImpl extends CustomHibernateDaoSupport implements StockDao {
	@Override
	public void save(StockDO stock) {
		getHibernateTemplate().save(stock);
	}

	@Override
	public void update(StockDO stock) {
		getHibernateTemplate().update(stock);
	}

	@Override
	public void delete(StockDO stock) {
		getHibernateTemplate().delete(stock);
	}
	
	@Override
	public List<StockDO> findStock(String customerId, String product){
		Criteria criteria = getSession().createCriteria(StockDO.class);
		if(StringUtils.isNotEmpty(customerId)){
			criteria.add(Expression.like("customerId", "%"+customerId+"%"));
		}
		
		if(StringUtils.isNotEmpty(product)){
			criteria.add(Expression.like("product", "%"+product+"%"));
		}
		return criteria.list();
	}
	
	@Override
	public List<StockDO> findStock(StockQuery stockQuery){
		Criteria criteria = getSession().createCriteria(StockDO.class);
		if(StringUtils.isNotEmpty(stockQuery.getCustomerId())){
			criteria.add(Expression.like("customerId", "%" +stockQuery.getCustomerId() + "%"));
		}
		if(StringUtils.isNotEmpty(stockQuery.getProduct())){
			criteria.add(Expression.eq("product", stockQuery.getProduct()));
		}
		if(StringUtils.isNotEmpty(stockQuery.getBatchNo())){
			criteria.add(Expression.eq("batchNo", stockQuery.getBatchNo()));
		}
		return criteria.list();
	}
}
/**
 * 
 */
package org.storagemanagement.DAO;

import java.util.List;

import org.storagemanagement.DO.ProductExportDO;
import org.storagemanagement.model.ExportRecordQuery;

/**
 * @author chengsen
 *
 */
public interface ProductExportDao {
	
	void save(ProductExportDO record);

	void update(ProductExportDO record);

	void delete(ProductExportDO record);

	List<ProductExportDO> find(ExportRecordQuery exportQuery);
}

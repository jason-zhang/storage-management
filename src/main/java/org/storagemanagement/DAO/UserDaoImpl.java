/**
 * 
 */
package org.storagemanagement.DAO;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.springframework.stereotype.Repository;
import org.storagemanagement.DO.UserDO;
import org.storagemanagement.model.UserQuery;
import org.storagemanagement.uti.CustomHibernateDaoSupport;

/**
 * @author chengsen.zhang
 * 
 */
@Repository("userDao")
public class UserDaoImpl extends CustomHibernateDaoSupport implements UserDao {
	@Override
	public void save(UserDO userDO) {
		getHibernateTemplate().save(userDO);
	}

	@Override
	public List<UserDO> findUser(UserQuery query) {
		Criteria criteria = getSession().createCriteria(UserDO.class);
		
		if(StringUtils.isNotEmpty(query.getUsername())){
			criteria.add(Expression.eq("username", query.getUsername()));
		}
		if(StringUtils.isNotEmpty(query.getPassword())){
			criteria.add(Expression.eq("password", query.getPassword()));
		}
		if(StringUtils.isNotEmpty(query.getLevel().getType())){
			criteria.add(Expression.eq("level", query.getLevel().getType()));
		}
	 return criteria.list();
	}

}
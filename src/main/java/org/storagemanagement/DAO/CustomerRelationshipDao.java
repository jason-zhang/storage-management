/**
 * 
 */
package org.storagemanagement.DAO;

import java.util.List;

import org.storagemanagement.DO.CustomerRelationshipDO;
import org.storagemanagement.model.CustomerRelationshipQuery;

/**
 * @author chengsen
 * 
 */
public interface CustomerRelationshipDao {

	void add(CustomerRelationshipDO record);

	List<CustomerRelationshipDO> find(CustomerRelationshipQuery query);
}
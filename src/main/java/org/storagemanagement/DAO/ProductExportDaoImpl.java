/**
 * 
 */
package org.storagemanagement.DAO;

import java.text.ParseException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.springframework.stereotype.Repository;
import org.storagemanagement.DO.ProductExportDO;
import org.storagemanagement.constant.StorageConstant;
import org.storagemanagement.model.ExportRecordQuery;
import org.storagemanagement.uti.CustomHibernateDaoSupport;

/**
 * @author Jason.zhang
 *
 */
@Repository("productExportDao")
public class ProductExportDaoImpl extends CustomHibernateDaoSupport implements  ProductExportDao{

	@Override
	public void save(ProductExportDO productReduceDO) {
		getHibernateTemplate().save(productReduceDO);
	}

	@Override
	public void update(ProductExportDO productReduceDO) {
		getHibernateTemplate().update(productReduceDO);
	}

	@Override
	public void delete(ProductExportDO productReduceDO) {
		getHibernateTemplate().delete(productReduceDO);
	}

	@Override
	public List<ProductExportDO> find(ExportRecordQuery exportQuery) {
		Criteria criteria = getSession().createCriteria(ProductExportDO.class);
		if(StringUtils.isNotEmpty(exportQuery.getCustomerId())){
			criteria.add(Expression.like("customerId", "%"+exportQuery.getCustomerId()+"%"));
		}
		
		if(StringUtils.isNotEmpty(exportQuery.getProduct())){
			criteria.add(Expression.like("product", "%"+exportQuery.getProduct()+"%"));
		}
		
		if(StringUtils.isNotEmpty(exportQuery.getExportDateFrom())){
			String exportDateFrom = exportQuery.getExportDateFrom();
			try {
				criteria.add(Expression.ge("exportTime", StorageConstant.dateFormatWithSeconds.parse(exportDateFrom)));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		if(StringUtils.isNotEmpty(exportQuery.getExportDateTo())){
			String exportDateTo = exportQuery.getExportDateTo() + " 23:59:59";
			try {
				criteria.add(Expression.le("exportTime",StorageConstant.dateFormatWithSeconds.parse(exportDateTo)));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return criteria.list();
	}
}

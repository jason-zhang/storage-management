/**
 * 
 */
package org.storagemanagement.DAO;

import java.util.List;

import org.storagemanagement.DO.UniversalDO;

/**
 * @author chengsen
 * 
 */
public interface UniversalDao {

	void save(UniversalDO universalDO);

	void delete(UniversalDO universalDO);

	List<UniversalDO> findByType(String type);
}
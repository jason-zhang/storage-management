/**
 * 
 */
package org.storagemanagement.DAO;

import java.sql.Date;
import java.text.ParseException;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.springframework.stereotype.Repository;
import org.storagemanagement.DO.ProductImportDO;
import org.storagemanagement.constant.StorageConstant;
import org.storagemanagement.model.ImportRecordQuery;
import org.storagemanagement.uti.CustomHibernateDaoSupport;

/**
 * @author Jason.zhang
 *
 */
@Repository("productImportDao")
public class ProductImportDaoImpl extends CustomHibernateDaoSupport implements  ProductImportDao{

	@Override
	public void save(ProductImportDO productAddDO) {
		getHibernateTemplate().save(productAddDO);
	}

	@Override
	public void update(ProductImportDO productAddDO) {
		getHibernateTemplate().update(productAddDO);
	}

	@Override
	public void delete(ProductImportDO productAddDO) {
		getHibernateTemplate().delete(productAddDO);
	}

	@Override
	public List<ProductImportDO> find(ImportRecordQuery importQuery) {
		Criteria criteria = getSession().createCriteria(ProductImportDO.class);
		
		if(StringUtils.isNotEmpty(importQuery.getCustomerId())){
			criteria.add(Expression.like("customerId", "%" + importQuery.getCustomerId()+"%"));
		}
		if(StringUtils.isNotEmpty(importQuery.getProduct())){
			criteria.add(Expression.like("product", "%" +importQuery.getProduct() +"%"));
		}
		if(StringUtils.isNotEmpty(importQuery.getBatchNo())){
			criteria.add(Expression.eq("batchNo", importQuery.getBatchNo() ));
		}
		if(StringUtils.isNotEmpty(importQuery.getImportDateFrom())){
			criteria.add(Expression.ge("importTime", Date.valueOf(importQuery.getImportDateFrom())));
		}
		if(StringUtils.isNotEmpty(importQuery.getImportDateTo())){
			String importDate = importQuery.getImportDateTo() + " 23:59:59";
			try {
				criteria.add(Expression.le("importTime", StorageConstant.dateFormatWithSeconds.parse(importDate)));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return criteria.list();
	}
}

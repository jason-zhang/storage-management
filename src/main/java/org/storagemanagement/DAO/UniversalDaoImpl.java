/**
 * 
 */
package org.storagemanagement.DAO;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Expression;
import org.springframework.stereotype.Repository;
import org.storagemanagement.DO.UniversalDO;
import org.storagemanagement.uti.CustomHibernateDaoSupport;

/**
 * @author chengsen
 * 
 */
@Repository("universalDao")
public class UniversalDaoImpl extends CustomHibernateDaoSupport implements UniversalDao {
	@Override
	public void save(UniversalDO universalDO) {
		getHibernateTemplate().save(universalDO);
	}

	@Override
	public void delete(UniversalDO universalDO) {
	}

	@Override
	public List<UniversalDO> findByType(String type) {
		Criteria criteria = getSession().createCriteria(UniversalDO.class);
		if(StringUtils.isNotEmpty(type)){
			criteria.add(Expression.eq("type", type));
		}
		return criteria.list();
	}
}
/**
 * 
 */
package org.storagemanagement.DAO;

import java.util.List;

import org.storagemanagement.DO.StockDO;
import org.storagemanagement.model.StockQuery;

/**
 * @author chengsen
 * 
 */
public interface StockDao {

	void save(StockDO stock);

	void update(StockDO stock);

	void delete(StockDO stock);

	List<StockDO> findStock(String customerId,String product);
	
	List<StockDO> findStock(StockQuery stockQuery);
}
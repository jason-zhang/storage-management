/**
 * 
 */
package org.storagemanagement.DAO;

import java.util.List;

import org.storagemanagement.DO.ProductImportDO;
import org.storagemanagement.model.ImportRecordQuery;

/**
 * @author chengsen
 *
 */
public interface ProductImportDao {
	
	void save(ProductImportDO record);

	void update(ProductImportDO record);

	void delete(ProductImportDO record);

	List<ProductImportDO> find(ImportRecordQuery importQuery);
}

/**
 * 
 */
package org.storagemanagement.DAO;

import java.util.List;

import org.storagemanagement.DO.UserDO;
import org.storagemanagement.model.UserQuery;

/**
 * @author chengsen.zhang
 * 
 */
public interface UserDao {

	void save(UserDO stock);

	List<UserDO> findUser(UserQuery query);
}
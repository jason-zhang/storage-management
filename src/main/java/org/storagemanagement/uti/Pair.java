/**
 * 
 */
package org.storagemanagement.uti;

/**
 * @author jason.zhang
 *
 */
public class Pair<U,T> {
	private U first;
	private T second;
	public Pair(U first,T second){
		this.first = first;
		this.second = second;
	}
	public U getFirst() {
		return this.first;
	}
	public void setFirst(U first) {
		this.first = first;
	}
	public T getSecond() {
		return this.second;
	}
	public void setSecond(T second) {
		this.second = second;
	}
	@Override
	public boolean equals(Object pair){
		return ((Pair) pair).getFirst().equals(this.first) && ((Pair) pair).getSecond().equals(this.second);
	}
	
	@Override
	public int hashCode(){
		return first.hashCode() + second.hashCode();
	}
}

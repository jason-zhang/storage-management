/**
 * 
 */
package org.storagemanagement.uti;

/**
 * @author chengsen
 *
 */
public enum OperationTypeEnum {
	BORROW,RETURN,BUY_SELL;
	public String getType() {
		String s = super.toString();
		return s.toLowerCase();
	}
}


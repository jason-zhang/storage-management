/**
 * 
 */
package org.storagemanagement.uti;

import org.storagemanagement.model.LoginVO;

/**
 * @author chengsen
 *
 */
public class LoginUtils {
	public static boolean isAuthed(LoginVO login){
		return login == null?false:login.isLogin();
	}
}

/*global Ext:false */

/*
 This file is part of Ext JS 4
 GNU General Public License Usage
 */
Ext.require([ '*' ]);

Ext.define('Storage.AccountStore', {
	extend : 'Ext.data.Store',
	fields : [ {
		name : 'customerId',
		type : 'string'
	}, {
		name : 'product',
		type : 'string'
	}, {
		name : 'batchNo',
		type : 'string'
	}, {
		name : 'quantity',
		type : 'string'
	} ],
	autoLoad : 'false',
	proxy : {
		type : 'ajax',
		url : 'rest/stock/get_products',
		reader : {
			type : 'json',
			root : 'checkAccount'
		}
	}
});

/** Account Table */
Ext.define('Storage.AccountTable', {
	extend : 'Ext.grid.Panel',
	id : "checkAccountRecordPanel",
	store : new Storage.AccountStore(),
	columns : [ {
		text : 'Customer',
		dataIndex : 'customerId',
		flex : 1
	}, {
		text : 'Product Type',
		dataIndex : 'product',
		flex : 1
	}, {
		text : 'Batch',
		dataIndex : 'batchNo',
		flex : 1
	}, {
		text : 'Quantity',
		dataIndex : 'quantity',
		flex : 1
	} ],
	height : 300,
	width : 800
});

function CheckAccountControler() {
}

/** create header */
CheckAccountControler.prototype.createHeader = function() {
	var queryConditonOfCheckAccount = Ext.create('Ext.container.Container', {
		id : 'queryConditonOfCheckAccount',
		layout : 'vbox',
		items : [{
					xtype : 'container',
					layout : 'hbox',
					items : [
							{
								id : 'customerIdForStock',
								xtype : 'textfield',
								name : 'name',
								fieldLabel : 'Customer ID'
							},
							{
								xtype : 'button',
								text : 'Query',
								listeners : {
									click : function(b, e) {
										Ext.getCmp('checkAccountRecordPanel').getStore().reload({
										    params: {'customerId': Ext.getCmp('customerIdForStock').getValue()}
										} );
									}
								}
							} ]
				}, {
					xtype : 'container',
					layout : 'hbox',
					items : [ {
						xtype : 'button',
						text : 'Save as Excel'
					} ]
				} ]
	});
	return queryConditonOfCheckAccount;
};

/** create body */
CheckAccountControler.prototype.createBody = function() {
	var checkAccountTable = Ext.create('Ext.container.Container', {
		id : 'checkAccountTable',
		items : [ new Storage.AccountTable() ]
	});
	return checkAccountTable;
};

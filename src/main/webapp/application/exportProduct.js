/*
 * product export logic.
 * Interface okay, logic okay
 */
Ext.require([ '*' ]);

var clickedRowNumber;

/** export product store model */
Ext.define('Storage.export.Store',{
	extend:'Ext.data.Store',
	storeId : 'productExportStore',
	fields : [ 'customerId', 'product', 'batchNo', 'quantity', 'importFrom', 'importTime' ],
	autoLoad: 'false',
	proxy: {
	    type: 'ajax',
	    url : 'rest/export/get_existing_products',
	    reader: {
	    	type: 'json',
	    	root:'exportRecord'
	    }
	}
});

/** pop up window of export product table */
Ext.define('Storage.export.popup.Window',{
	extend:'Ext.window.Window',
	applyTo : 'hello-win',
	layout : 'fit',
	modal : true,
	items : [ {
		xtype : 'panel',
		layout : 'vbox',
		items : [ {
			xtype : 'panel',
			layout : 'hbox',
			margin : '5 5 5 5',
			border : false,
			items : [ {
				xtype : 'label',
				text : 'Customer Id:',
				margin : '5 5 5 5'
			}, {
				id:'customerInConfirmBox',
				xtype : 'label',
				text : '',
				margin : '5 5 5 5'
			}, {
				xtype : 'label',
				text : 'Batch No:',
				margin : '5 5 5 5'
			}, {
				id:'batchInConfirmBox',
				xtype : 'label',
				text : '',
				margin : '5 5 5 5'
			} ]
		}, {
			xtype : 'panel',
			layout : 'hbox',
			margin : '10 10 10 10',
			border : false,
			items : [ {
				xtype : 'label',
				text : 'Product Type:',
				margin : '5 5 5 5'
			}, {
				id:'productInConfirmBox',
				xtype : 'label',
				text : '',
				margin : '5 5 5 5'
			}, {
				xtype : 'label',
				text : 'quantity:',
				margin : '5 5 5 5'
			}, {
				id:'quantityInConfirmBox',
				xtype : 'numberfield',
				margin : '5 5 5 5',
				allowNegative : false,
				minValue : 1,
				maxValue : 1000
			} ]
		}, {
			xtype : 'panel',
			layout : 'hbox',
			margin : '5 5 5 5',
			border : false,
			items : [ {
				xtype : 'label',
				text : 'Operator:',
				margin : '5 5 5 5'
			}, {
				id:'operatorInConfirmBox',
				xtype : 'label',
				text : 'Jason',
				margin : '5 5 5 5'
			}, {
				xtype : 'label',
				text : 'Export Date:',
				margin : '5 5 5 5'
			}, {
				id:'exportTimeInConfirmBox',
				xtype : 'datefield',
				anchor : '100%'
			} ]
		}, {
			xtype : 'panel',
			layout : 'hbox',
			margin : '5 5 5 300',
			border : false,
			items : [ {
				xtype : 'button',
				text : 'Add',
				listeners: {
					click: function (button,event) {
						if(Ext.getCmp('exportTimeInConfirmBox').getValue() == null || Ext.getCmp('quantityInConfirmBox').getValue() ==null ){
							Ext.Msg.alert("", 'All fields can not be empty', null);
							return;
						}
						
						var rowData = {
							"customerId" : Ext.getCmp('customerInConfirmBox').text,
							"product" :  Ext.getCmp('productInConfirmBox').text,
							"batchNo" : Ext.getCmp('batchInConfirmBox').text,
							"quantity": Ext.getCmp('quantityInConfirmBox').getValue(),
							"exportTime" :Ext.getCmp('exportTimeInConfirmBox').getRawValue()
						};
						
						var store =  Ext.getCmp("productExportTableToSumbitPanel").getStore();
						store.insert(store.getCount(), rowData);
						var recordStore = Ext.getCmp('existingRecordsOfExportTable').getStore();
						var currentQuan = recordStore.getAt(clickedRowNumber).data.quantity;
						recordStore.getAt(clickedRowNumber).set('quantity',currentQuan - Ext.getCmp('quantityInConfirmBox').getValue());
						
						button.ownerCt.ownerCt.ownerCt.close();
				   	} }
			} ]
		} ]
	} ]
});

/* export table */
Ext.define('Storage.export.Table',{
	extend:'Ext.grid.Panel',
	id: 'existingRecordsOfExportTable',
	store : new Storage.export.Store(),
	columns : [ {
		text : 'Customer Id',
		dataIndex : 'customerId',
		flex : 1
	}, {
		text : 'Product Type',
		dataIndex : 'product',
		flex : 1
	}, {
		text : 'Batch No',
		dataIndex : 'batchNo',
		flex : 1
	}, {
		text : 'Quantity',
		dataIndex : 'quantity',
		flex : 1
	}, {
		text : 'Import Time',
		dataIndex : 'importTime',
		flex : 1,
		renderer: function(value) {
			var exportsTime = new Date(value);
			return Ext.Date.format(exportsTime,'Y-m-d');
		}
	} ],
	height : 300,
	width : 950,
	listeners : {
		itemdblclick : function(dv, record, item, index, e) {
			var win = new Storage.export.popup.Window();
			clickedRowNumber = index;
			Ext.getCmp('customerInConfirmBox').text = record.data.customerId;
			Ext.getCmp('productInConfirmBox').text = record.data.product;
			Ext.getCmp('batchInConfirmBox').text = record.data.batchNo;
			Ext.getCmp('quantityInConfirmBox').setValue(record.data.quantity);
			Ext.getCmp('quantityInConfirmBox').setMaxValue(record.data.quantity);
			win.show();
		}
	}
});

// data need to confirm to submit
Ext.define('Storage.export.submit.Table',{
	extend:'Ext.grid.Panel',
	id:'productExportTableToSumbitPanel',
	store : Ext.create('Ext.data.Store',{
		storeId : 'productExportStore',
		fields : [ 'customerId', 'product', 'batchNo', 'quantity', 'exportTime' ],
		listeners : {
			datachanged : function(obj,option) {
				var totalCount = 0;
				for(var index  = 0; index<obj.getCount() ; index++ ){
					totalCount += obj.getAt(index).data.quantity;
				}
				Ext.getCmp("exportProductTotal").text = totalCount;
			}
		}
		}),
		columns : [ {
			text : 'Customer Id',
			dataIndex : 'customerId',
			flex : 1
		}, {
			text : 'Product Type',
			dataIndex : 'product',
			flex : 1
		}, {
			text : 'Batch No',
			dataIndex : 'batchNo',
			flex : 1
		}, {
			text : 'Quantity',
			dataIndex : 'quantity',
			flex : 1
		}, {
			text : 'Export Time',
			dataIndex : 'exportTime',
			flex : 1
		} ],
		width : 800
});

/* export product */
function ExportProductController() {
	
}

/* create export product panel */
ExportProductController.prototype.createHeader = function(){
	var operationOfProductExport = Ext.create('Ext.container.Container', {
		id : 'operationOfProductExport',
		layout : 'hbox',
		items : [ {
			xtype : 'container',
			layout : 'hbox',
			items : [ {
				id:"exportProductCustomerId",
				xtype : 'textfield',
				fieldLabel : 'Customer ID',
			},{
				id:"exportProductProductType",
				xtype : 'textfield',
				fieldLabel : 'Product Type',
			}, {
				xtype : 'button',
				text : 'Go',
				listeners : {
					click : function(b, e) {
						Ext.getCmp('existingRecordsOfExportTable').getStore().reload({
						    params: {'customerId': Ext.getCmp('exportProductCustomerId').getValue(),
						    		 'product': Ext.getCmp('exportProductProductType').getValue()}
						});
					}
				}
			} ]
		} ]
	});
	return operationOfProductExport;
};

// create upper body
ExportProductController.prototype.createUpperBody = function(){
	var exportTable = new Storage.export.Table();
	
	var upperTable = Ext.create('Ext.panel.Panel', {
		id : 'productExportTable',
		title:'Existing Product Record',
		items : [ exportTable ]
	});
	
	exportTable.getStore().load({
		params: {'customerId': ""}
	});
	
	return upperTable;
};

// create lower body
ExportProductController.prototype.createLowerBody = function(){
	var lowerbody = Ext.create('Ext.panel.Panel', {
		id : 'productExportTableToSumbit',
		title:'Confirm Exports Products',
		items : [ new Storage.export.submit.Table() ]
	});
	return lowerbody;
};

/* create bottom */
ExportProductController.prototype.createBottom = function(){
	var bottomPanel = Ext.create('Ext.panel.Panel', {
		id : 'productExportTableFootBar',
		items : [ {
			xtype : 'button',
			text : 'Delete: ',
			listeners : {
				click : function(b, e) {
					var store = Ext.getCmp("productExportTableToSumbitPanel").getStore();
					var selectedRecord = Ext.getCmp("productExportTableToSumbitPanel").getSelectionModel().getSelection()[0];
					var row = store.indexOf(selectedRecord);
					store.removeAt(row);
				}
			}
		},{
			xtype:'button',
			text : 'Submit',
			margin:'10,0,0,500',
			listeners: {
				click: function (b,e) {
					var store = Ext.getCmp("productExportTableToSumbitPanel").getStore();
					if(store.getCount() == 0){
						Ext.Msg.alert("", 'Please add data first', null);
						return;
					}
					Ext.Msg.confirm('',"Do you want to submit?", function(btn, text){
						if (btn == 'yes') {
							var successCount = 0;
					    	for(var index = 0; index<store.getCount(); index++){
					    		var data = store.getAt(index).getData();
					    		Ext.Ajax.request({
					    			url: 'rest/export/add_record',
					    			method: 'POST',
					    			async: false, 
					    			jsonData: data,
					    			success: function() {
					    				successCount = successCount + 1;
					    				if(successCount == store.getCount()){
						    				Ext.Msg.alert("", 'Submit Successfully', null);
						    				store.removeAll();
					    				}
					    			},
					    			failure: function() {
					    				Ext.Msg.alert("", 'Submit Failed', null);
					    			}
					    		});
					    	}
							
						}
					});
			   	}
			}
		}, {
			id : 'totalNumber',
			xtype : 'label',
			margin:'0 10 0 300',
			text : 'total: '
		},{
			id : 'exportProductTotal',
			xtype : 'label',
			text : ''
		},{
			id : 'emtpyLableForExport',
			xtype : 'label',
			margin:'0 10 0 330',
			text : ''
		} ]
	});
	return bottomPanel;
};


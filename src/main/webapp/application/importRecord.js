/*global Ext:false */

/*
 This file is part of Ext JS 4
 GNU General Public License Usage
 */
Ext.require([ '*' ]);

/* import record store model */
Ext.define('Storage.import.record.Store', {
	extend : 'Ext.data.Store',
	fields : ['customerId', 'product', 'batchNo', 'storage', 'quantity',
			'importFrom', 'importTime','transport','plateNo' ],
	autoLoad : 'false',
	proxy : {
		type : 'ajax',
		url : 'rest/import/get_records',
		reader : {
			type : 'json',
			root : 'importRecord'
		}
	}
});

Ext.define('Storage.import.record.Table', {
	extend : 'Ext.grid.Panel',
	id : 'importRecordTablePanel',
	store : new Storage.import.record.Store(),
	height : 450,
	width : 950,
	columns : [ /*{
		text : 'Customer Name',
		dataIndex : 'customerName'
	},*/{
		text : 'Customer ID',
		dataIndex : 'customerId'
	}, {
		text : 'Product Type',
		dataIndex : 'product',
		flex : 1
	}, {
		text : 'Batch',
		dataIndex : 'batchNo',
		flex : 1
	}, {
		text : 'Storage',
		dataIndex : 'storage',
		flex : 1
	}, {
		text : 'Quantity',
		dataIndex : 'quantity',
		flex : 1
	}, {
		text : 'From',
		dataIndex : 'importFrom',
		flex : 1
	},{
		text : 'Transport',
		dataIndex : 'transport',
		flex : 1
	}, {
		text : 'Plate No',
		dataIndex : 'plateNo',
		flex : 1
	},  {
		text : 'Import Time',
		dataIndex : 'importTime',
		flex : 1,
		renderer: function(value) {
			var importTime = new Date(value);
			return Ext.Date.format(importTime,'Y-m-d');
		}
	} ]
});

/* Define a controller to control the create of Import record Controller */
function ImportRecordController() {

}

// create import record condition panel
ImportRecordController.prototype.createHeader = function() {
	var queryConditonOfImportRecord = Ext.create('Ext.panel.Panel', {
		id : 'queryConditonOfImportRecord',
		layout : 'vbox',
		items : [{
					xtype : 'container',
					layout : 'hbox',
					margin : '10,10,10,10',
					items : [ {
						id:'customerId',
						xtype : 'textfield',
						name : 'name',
						fieldLabel : 'Customer ID'
					}, {
						id:'productType',
						xtype : 'combo',
						fieldLabel : 'Product Type',
						store : [ 'a', 'b' ],
						queryMode : 'local',
						displayField : 'name',
						valueField : 'abbr',
						listeners : {}
					}, {
						id:'batchNo',
						xtype : 'textfield',
						name : 'name',
						fieldLabel : 'Batch Number'
					} ]
				},
				{
					xtype : 'container',
					layout : 'hbox',
					margin : '10,10,10,10',
					items : [ {
						id:'importDateFrom',
						xtype : 'datefield',
						anchor : '100%',
						fieldLabel : 'Import Date From'
					}, {
						id:'importDateTo',
						xtype : 'datefield',
						anchor : '100%',
						fieldLabel : 'To',
						name : 'from_date'
					} ]
				},
				{
					xtype : 'container',
					layout : 'hbox',
					align : 'right',
					items : [{
								xtype : 'button',
								text : 'Query',
								margin : '10,10,10,10',
								listeners : {
									click : function(b, e) {
										Ext.getCmp('importRecordTablePanel').getStore().reload({
												    params: {'customerId': Ext.getCmp('customerId').getValue(), 
												    	     'product': Ext.getCmp('productType').getValue(),
												    	     'batchNo':Ext.getCmp('batchNo').getValue(),
												    	     'importDateFrom':Ext.Date.format( Ext.getCmp('importDateFrom').getValue(), 'Y-m-d'),
												    	     'importDateTo':Ext.Date.format( Ext.getCmp('importDateTo').getValue(), 'Y-m-d')}
											});
									}
								}
							}, {
								xtype : 'button',
								margin : '10,10,10,10',
								text : 'Save as Excel'
							} ]
				} ]
	});
	return queryConditonOfImportRecord;
}

// create import record table
ImportRecordController.prototype.createBody = function() {
	var importTableStore  = new Storage.import.record.Table();
	var importRecordTable = Ext.create('Ext.panel.Panel', {
		id : 'importRecord',
		items : [ importTableStore ]
	});
	importTableStore.getStore().load({params:{}});
	return importRecordTable;
}

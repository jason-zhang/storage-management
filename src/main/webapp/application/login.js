Ext.require([ '*' ]);

Ext.QuickTips.init();

// create import record condition panel
Ext.onReady(function() {

	Ext.QuickTips.init();
	
	var loginForm = new Ext.FormPanel({
		id : 'loginForm',
		renderTo : document.body,
		frame : true,
		title : '易达通货物管理 - 登录系统',
		width : 400,
		items : [ {
			id : "username",
			xtype : 'textfield',
			fieldLabel : 'User Name',
			name : 'uname',
			width : 180,
			allowBlank : false,
			blankText : 'User Name can not be null',
			minLength : 6,
			minLengthText : '用户名的长度为[6-20]',
			maxLength : 20,
			maxLengthText : '用户名的长度为[6-20]'
		}, {
			id : "password",
			xtype : 'textfield',
			inputType : 'password',
			fieldLabel : 'password:',
			name : 'pwd',
			width : 180,
			allowBlank : false,
			blankText : 'password can not be null',
			minLength : 6,
			minLengthText : 'password lengh should be[6-20]',
			maxLength : 20,
			maxLengthText : 'password lengh should be [6-20]'
		} ],
		buttons : [ {
			text : 'Login',
			type : 'button',
			listeners : {
				click : function(b, e) {
					Ext.Ajax.request({
						url : "rest/user/authenticateUser", // 请求的地址
						params : {
							'username' : Ext.getCmp('username').getValue(),
							'password' : Ext.getCmp('password').getValue(),
						}, // 发送的参数
						success : function(response, option) {
							loginForm.hide();
						},
						failure : function() {
							Ext.Msg.alert("Bad", "Wrong！");
						}
					});
				}
			}
		} ]
	});

	var viewPort = new Ext.Viewport({
		id : "loginViewport",
		renderTo : 'body',
		width : 400,
		height : 400,
		layout : {
			type : 'vbox',
			align : 'center',
			pack : 'center'
		},
		items : [ loginForm ]
	});
});

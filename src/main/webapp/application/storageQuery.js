/*global Ext:false */

/*
 This file is part of Ext JS 4
 GNU General Public License Usage
 */
Ext.require([ '*' ]);

/*  */
Ext.define('Strorage.query.Store',{
	extend:'Ext.data.Store',
	storeId : 'simpsonsStore',
	fields : [ 'customerId', 'product', 'quantityWithBatchList'],
	autoLoad: 'true',
    proxy: {
        type: 'ajax',
        url : 'rest/stock/get_records_grouped_by_product',
        reader: {
        	type: 'json',
        	root:'storageQuery'
        }
    }
}); 

/** storage query table */
Ext.define('Storage.query.Table',{
	extend:'Ext.grid.Panel',

	id: "storageQueryPanel",
	store : new Strorage.query.Store(),
	columns : [ {
		text : 'Customer ID',
		dataIndex : 'customerId',
		flex : 1
	}, {
		text : 'Product Type',
		dataIndex : 'product',
		flex : 1
	}, {
		text : 'Quantity',
		dataIndex : 'quantityWithBatchList',
		flex : 1,
		renderer: function(value) {
			var returnText = value.quantity;
			for(var index = 0; index<value.batches.length;index++ ){
				returnText += "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + value.batches[index].quantity + " -- "+ value.batches[index].batchNo;
			}
			return returnText;
        }
	} ],
	height : 300,
	width : 800
});


/* Define a controller to control the create of Import  record Controller */
function StorageQueryController() {
	
}

// create storage query condition panel
StorageQueryController.prototype.createHeader = function() {
	var queryConditonOfStorageQuery = Ext.create('Ext.container.Container', {
		id : 'queryConditonOfStorageQuery',
		layout : 'vbox',
		items : [ {
			xtype : 'container',
			layout : 'hbox',
			items : [ {
				id : 'storageQueryId',
				xtype : 'textfield',
				name : 'name',
				fieldLabel : 'Customer ID'
			}, {
				xtype : 'button',
				text : 'Go',
				listeners : {
					click : function(b, e) {
						Ext.getCmp('storageQueryPanel').getStore().reload({
						    params: {'customerId': Ext.getCmp('storageQueryId').getValue()}
						} );
					}
				}
			} ]
		}, {
			xtype : 'container',
			layout : 'hbox',
			items : [ {
				xtype : 'button',
				text : 'Save as Excel'
			} ]
		} ]
	});
	return queryConditonOfStorageQuery;
};

// create storage query body
StorageQueryController.prototype.createBody = function() {
	var storageQueryTableInner = new Storage.query.Table();
	var storageQueryTable = Ext.create('Ext.container.Container', {
		id : 'storageQueryTable',
		items : [ storageQueryTableInner ]
	});
	storageQueryTableInner.getStore().load({params:{}});
	return storageQueryTable
}
/*global Ext:false */

/*
 This file is part of Ext JS 4
 GNU General Public License Usage
 */
Ext.require([ '*' ]);

Ext.define('Storage.product.borrow.Store', {
	extend : 'Ext.data.Store',
	storeId : 'simpsonsStore',
	fields : [ 'productFrom', 'productTo', 'product', 'batchNo', 'quantity', 'actionTime' ],
	autoLoad : 'false'
});

Ext.define('Storage.product.borrow.Table', {
	extend : 'Ext.grid.Panel',
	id: 'productBorrowTable',
	store : new Storage.product.borrow.Store(),
	height : 450,
	width : 950,
	columns : [ {
		text : 'from',
		dataIndex : 'productFrom',
		flex : 1
	}, {
		text : 'to',
		dataIndex : 'productTo',
		flex : 1
	}, {
		text : 'productType',
		dataIndex : 'product',
		flex : 1
	}, {
		text : 'Batch No',
		dataIndex : 'batchNo',
		flex : 1
	}, {
		text : 'quantity',
		dataIndex : 'quantity',
		flex : 1
	}, {
		text : 'actionTime',
		dataIndex : 'actionTime',
		flex : 1
	} ]

});

function ProductBorrowController() {
}

ProductBorrowController.prototype.createHeader = function() {

	var operationOfBorrow = Ext.create('Ext.container.Container', {
		id : 'operationOfBorrow',
		layout : 'hbox',
		items : [ {
			xtype : 'container',
			layout : 'vbox',
			items : [ {
				id : 'productFrom',
				xtype : 'textfield',
				name : 'productFrom',
				fieldLabel : 'From',
				allowBlank : false
			}, {
				id : 'quantity',
				xtype : 'textfield',
				name : 'quantity',
				fieldLabel : 'Quantity'
			} ]
		}, {
			xtype : 'container',
			layout : 'vbox',
			items : [ {
				id : 'productTo',
				xtype : 'textfield',
				name : 'productTo',
				fieldLabel : 'To',
				allowBlank : false
			}, {
				id : 'batchNo',
				xtype : 'textfield',
				name : 'name',
				fieldLabel : 'Batch No',
				allowBlank : false
			} ]
		}, {
			xtype : 'container',
			layout : 'vbox',
			items : [ {
				id : 'product',
				xtype : 'combo',
				fieldLabel : 'Product Type',
				store : [ 'apt1', 'apt2' ]
			}, {
				xtype : 'container',
				layout : 'hbox',
				items : [ {
					id : 'borrowTime',
					xtype : 'datefield',
					anchor : '100%',
					fieldLabel : 'Date',
					name : 'Date',
					maxValue : new Date()
				},{
					xtype : 'button',
					id : 'buySellFootBar',
					text : 'Add',
					listeners : {
						click : function(b, e) {
							var rowData = {'productFrom': Ext.getCmp('productFrom').getValue(), 
						    	     'productTo': Ext.getCmp('productTo').getValue(),
						    	     'batchNo':Ext.getCmp('batchNo').getValue(),
						    	     'product':Ext.getCmp('product').getValue(),
						    	     'quantity':Ext.getCmp('quantity').getValue(),
						    	     'actionTime':Ext.Date.format(Ext.getCmp('borrowTime').getValue(),'Y-m-d') };
								if( rowData.borrowFrom=="" || rowData.borrowTo=="" || rowData.batchNo=="" ||
										rowData.product =="" || rowData.quantity=="" || rowData.actionTime=="" ){
									Ext.Msg.alert("", 'All fields can not be empty', null);
									return ;
								}
								
								var store = Ext.getCmp("productBorrowTable").getStore();
								store.insert(store.getCount(),rowData);
						}
					}
				},{
					xtype : 'button',
					text : 'Delete: ',
					listeners : {
						click : function(b, e) {
							var store = Ext.getCmp("productBorrowTable").getStore();
							var selectedRecord = Ext.getCmp("productBorrowTable").getSelectionModel().getSelection()[0];
							var row = store.indexOf(selectedRecord);
							store.removeAt(row);
						}
					}
				} ]
			} ]
		} ]
	});
	return operationOfBorrow;
};

ProductBorrowController.prototype.createBody = function() {

	var borrowTable = Ext.create('Ext.container.Container', {
		id : 'borrowTable',
		items : [ new Storage.product.borrow.Table() ]
	});
	return borrowTable;
};

//Create import product table
ProductBorrowController.prototype.createBottom = function() {
	var bottomToolbar = Ext.create('Ext.button.Button', {
		id : 'borrowFootBar',
		text : 'Submit',
		listeners : {
			click : function(b, e) {
				var store = Ext.getCmp("productBorrowTable").getStore();
				if(store.getCount() == 0){
					Ext.Msg.alert("", 'Please add data first', null);
					return;
				}
				Ext.Msg.confirm('', "Do you want to submit?", function(btn,text) {
					if (btn == 'yes') {
						var successCount = 0;
				    	for(var index = 0; index<store.getCount(); index++){
				    		var data = store.getAt(index).getData();
				    		var rowData = {"productFrom":data.productFrom,"productTo":data.productTo, "product":data.product ,"batchNo":data.batchNo ,"quantity":data.quantity,  "actionTime":data.actionTime};
				    		Ext.Ajax.request({
				    			url: 'rest/customer_relationship/borrow/add_borrow_record',
				    			method: 'POST',
				    			async: false, 
				    			jsonData: rowData,
				    			success: function() {
				    				successCount = successCount + 1;
				    				if(successCount == store.getCount()){
					    				Ext.Msg.alert("", 'Submit Successfully', null);
					    				store.removeAll();
				    				}
				    			},
				    			failure: function() {
				    				Ext.Msg.alert("", 'Submit Failed', null);
				    			}
				    		});
				    	}
					}
				});
			}
		}
	});
	return bottomToolbar;
}

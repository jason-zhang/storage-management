/*global Ext:false */

/*
 This file is part of Ext JS 4
 GNU General Public License Usage
 */
Ext.require([ '*' ]);

Ext.define('Storage.product.buysell.Store', {
	extend : 'Ext.data.Store',
	storeId : 'simpsonsStore',
	fields : [ 'buyFrom', 'sellTo', 'product', 'batchNo', 'quantity', 'actionTime' ],
	autoLoad : 'false'
});

Ext.define('Storage.product.buysell.Table', {
	extend : 'Ext.grid.Panel',
	id: 'productBuySellTable',
	store : new Storage.product.buysell.Store(),
	height : 450,
	width : 950,
	columns : [ {
		text : 'Buy From',
		dataIndex : 'buyFrom',
		flex : 1
	}, {
		text : 'Sell To',
		dataIndex : 'sellTo',
		flex : 1
	}, {
		text : 'Product Type',
		dataIndex : 'product',
		flex : 1
	}, {
		text : 'Batch No',
		dataIndex : 'batchNo',
		flex : 1
	}, {
		text : 'quantity',
		dataIndex : 'quantity',
		flex : 1
	}, {
		text : 'actionTime',
		dataIndex : 'actionTime',
		flex : 1
	} ]

});

function ProductBuySellController() {
}

ProductBuySellController.prototype.createHeader = function() {

	var operationOfBuySell = Ext.create('Ext.container.Container', {
		id : 'operationOfBuySell',
		layout : 'hbox',
		items : [ {
			xtype : 'container',
			layout : 'vbox',
			items : [ {
				id : 'buyFrom',
				xtype : 'textfield',
				name : 'productFrom',
				fieldLabel : 'From',
				allowBlank : false
			}, {
				id : 'quantity',
				xtype : 'textfield',
				name : 'quantity',
				fieldLabel : 'Quantity'
			} ]
		}, {
			xtype : 'container',
			layout : 'vbox',
			items : [ {
				id : 'sellTo',
				xtype : 'textfield',
				name : 'productTo',
				fieldLabel : 'To',
				allowBlank : false
			}, {
				id : 'batchNo',
				xtype : 'textfield',
				name : 'name',
				fieldLabel : 'Batch No',
				allowBlank : false
			} ]
		}, {
			xtype : 'container',
			layout : 'vbox',
			items : [ {
				id : 'product',
				xtype : 'combo',
				fieldLabel : 'Product Type',
				store : [ 'apt1', 'apt2' ]
			}, {
				xtype : 'container',
				layout : 'hbox',
				items : [ {
					id : 'buysellTime',
					xtype : 'datefield',
					anchor : '100%',
					fieldLabel : 'Date',
					name : 'Date',
					maxValue : new Date()
				},{
					xtype : 'button',
					id : 'buySellFootBar',
					text : 'Add',
					listeners : {
						click : function(b, e) {
							var rowData = {'buyFrom': Ext.getCmp('buyFrom').getValue(), 
						    	     'sellTo': Ext.getCmp('sellTo').getValue(),
						    	     'batchNo':Ext.getCmp('batchNo').getValue(),
						    	     'product':Ext.getCmp('product').getValue(),
						    	     'quantity':Ext.getCmp('quantity').getValue(),
						    	     'actionTime':Ext.Date.format(Ext.getCmp('buysellTime').getValue(),'Y-m-d') };
								if( rowData.buysellFrom=="" || rowData.buysellTo=="" || rowData.batchNo=="" ||
										rowData.product =="" || rowData.quantity=="" || rowData.actionTime=="" ){
									Ext.Msg.alert("", 'All fields can not be empty', null);
									return ;
								}
								
								var store = Ext.getCmp("productBuySellTable").getStore();
								store.insert(store.getCount(),rowData);
						}
					}
				},{
					xtype : 'button',
					text : 'Delete: ',
					listeners : {
						click : function(b, e) {
							var store = Ext.getCmp("productBuySellTable").getStore();
							var selectedRecord = Ext.getCmp("productBuySellTable").getSelectionModel().getSelection()[0];
							var row = store.indexOf(selectedRecord);
							store.removeAt(row);
						}
					}
				} ]
			} ]
		} ]
	});
	return operationOfBuySell;
};

ProductBuySellController.prototype.createBody = function() {

	var buysellTable = Ext.create('Ext.container.Container', {
		id : 'buysellTable',
		items : [ new Storage.product.buysell.Table() ]
	});
	return buysellTable;
};

//Create import product table
ProductBuySellController.prototype.createBottom = function() {
	var bottomToolbar = Ext.create('Ext.button.Button', {
		id : 'buysellFootBar',
		text : 'Submit',
		listeners : {
			click : function(b, e) {
				var store = Ext.getCmp("productBuySellTable").getStore();
				if(store.getCount() == 0){
					Ext.Msg.alert("", 'Please add data first', null);
					return;
				}
				Ext.Msg.confirm('', "Do you want to submit?", function(btn,text) {
					if (btn == 'yes') {
						var successCount = 0;
				    	for(var index = 0; index<store.getCount(); index++){
				    		var data = store.getAt(index).getData();
				    		var rowData = {"productFrom":data.buyFrom,"productTo":data.sellTo, "product":data.product ,"batchNo":data.batchNo ,"quantity":data.quantity,  "actionTime":data.actionTime};
				    		Ext.Ajax.request({
				    			url: 'rest/customer_relationship/buysell/add_buysell_record',
				    			method: 'POST',
				    			async: false, 
				    			jsonData: rowData,
				    			success: function() {
				    				successCount = successCount + 1;
				    				if(successCount == store.getCount()){
					    				Ext.Msg.alert("", 'Submit Successfully', null);
					    				store.removeAll();
				    				}
				    			},
				    			failure: function() {
				    				Ext.Msg.alert("", 'Submit Failed', null);
				    			}
				    		});
				    	}
					}
				});
			}
		}
	});
	return bottomToolbar;
}

/*global Ext:false */

/*
 This file is part of Ext JS 4
 GNU General Public License Usage
 */
Ext.require([ '*' ]);

/**
 * storage in out query record
 */
Ext.define('Storage.record.inout.Store', {
	extend : 'Ext.data.Store',
	storeId : 'simpsonsStore',
	fields : [ 'customerId', 'product', 'quantityWithRecordList' ],
	autoLoad: 'true',
	proxy: {
	    type: 'ajax',
	    url : 'rest/stock/get_records_with_import_export_record',
	    reader: {
	    	type: 'json',
	    	root:'queryStorageInout'
	    }
	}
});

/**
 * Storage query panel
 */
Ext.define('Storage.record.inout.record.Table', {
	extend : 'Ext.grid.Panel',
	id : 'recordInoutRecordQuery',
	store : new Storage.record.inout.Store(),
	height : 400,
	width : 950,
	columns : [ {
		text : 'Customer Name',
		dataIndex : 'customerId',
		flex : 1
	}, {
		text : 'Product Type',
		dataIndex : 'product',
		flex : 1
	}, {
		text : 'Quantity',
		dataIndex : 'quantityWithRecordList',
		flex : 1,
		renderer: function(value) {
			var totalQuantity = 0;
			var text = '';
			for(var index = 0; index< value.length; index++){
				var quantityItem = value[index].quantity;
				if(value[index].import){
					totalQuantity  += quantityItem;
					quantityItem =  "+" + quantityItem;
				}else{
					totalQuantity  -= quantityItem;
					quantityItem =  "-" + quantityItem;
				}
				text += "<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+ quantityItem ;
				text += ' -- ' +value[index].batchNo + ' -- ' +  value[index].importExportDate;
			}
			text = totalQuantity + text;
            return text; 
        }
	}]
});

/**
 * 
 */
function RecordInOutQueryController() {
}

/**
 * 
 */
RecordInOutQueryController.prototype.createHeader = function() {
	var queryConditonOfQueryRecordInOut = Ext.create('Ext.container.Container',{
				id : 'queryConditonOfQueryRecordInOut',
				layout : 'vbox',
				items : [ {
					xtype : 'container',
					layout : 'hbox',
					items : [ {
						id : "customerIdForStockQuery",
						xtype : 'textfield',
						name : 'name',
						fieldLabel : 'Customer ID',
						allowBlank : false
					}, {
						id:'productForStockQuery',
						xtype : 'combo',
						fieldLabel : 'productType',
						store : [ 'a', 'b' ],
						queryMode : 'local',
						displayField : 'name',
						valueField : 'abbr',
					}, {
						id : 'batchNoForStockQuery',
						xtype : 'textfield',
						name : 'name',
						fieldLabel : 'Batch Number',
						allowBlank : false
					} ]
				}, {
					xtype : 'container',
					layout : 'hbox',
					items : [ {
						id: 'importDateFromForStockQuery',
						xtype : 'datefield',
						anchor : '100%',
						fieldLabel : 'Import date From',
						// name : 'from_date',
						maxValue : new Date()
					}, {
						id: 'importDateToForStockQuery',
						xtype : 'datefield',
						anchor : '100%',
						fieldLabel : 'To',
						name : 'from_date',
						maxValue : new Date()
					},{
						xtype : 'container',
						layout : 'hbox',
						items : [ {
							xtype : 'button',
							text : 'Go',
							listeners : {
								click : function(b, e) {
									Ext.getCmp('recordInoutRecordQuery').getStore().reload({
									    params: {'customerId': Ext.getCmp('customerIdForStockQuery').getValue(),
									    	'product': Ext.getCmp('productForStockQuery').getValue(),  
									    	'batchNo': Ext.getCmp('batchNoForStockQuery').getValue(),
									    	"importDateFrom" : Ext.Date.format(Ext.getCmp('importDateFromForStockQuery').getValue(),'Y-m-d'),
									    	"importDateTo" : Ext.Date.format(Ext.getCmp('importDateToForStockQuery').getValue(),'Y-m-d')}
									} );
								}
							}
						}, {
							xtype : 'button',
							text : 'Save as Excel'
						} ]
					} ]
				} ]
			});
	return queryConditonOfQueryRecordInOut;
}

/**
 * 
 */
RecordInOutQueryController.prototype.createBody = function() {
	var queryRecordInoutTableInner = new Storage.record.inout.record.Table();
	
	var queryRecordInOutTable = Ext.create('Ext.container.Container', {
		id : 'queryRecordInOutTable',
		items : [ queryRecordInoutTableInner ]
	});
	queryRecordInoutTableInner.getStore().load({
	    params: {}
	} );
	return queryRecordInOutTable;
}

/*global Ext:false */

/*
 This file is part of Ext JS 4
 GNU General Public License Usage
 */
Ext.require([ '*' ]);

var importProductController = new ImportProductController();
var importRecordController = new ImportRecordController();
var exportProductController = new ExportProductController();
var exportRecordController = new ExportRecordController();
var checkAccountControler = new CheckAccountControler();
var storageQueryController = new StorageQueryController();
var recordInOutQueryController = new RecordInOutQueryController();
var productBorrowController = new ProductBorrowController();
var productReturnController = new ProductReturnController();
var productSellBuyController = new ProductBuySellController();
var customerRelationshipRecordQueryController = new CustomerRelationshipRecordQueryController();
Ext.onReady(function() {
			// create the Grid
			Ext.create( 'Ext.Viewport',
			{
				layout : {
					type : 'border',
					padding : 5
				},
				defaults : {
					split : true
				},
				items : [{
						region : 'west',
						collapsible : true,
						title : 'Storage System',
						split : true,
						width : '25%',
						height : "100%",
						minWidth : 100,
						minHeight : 140,
						items : [ Ext.create('Ext.tree.Panel',
								{ 	rootVisible : false,
									width : "100%",
									height : "100%",
									listeners : { itemclick : function(
										s,record) {
										var itemId = record.getId();
										var title = record.getData().text;
										var centerPanel = Ext.getCmp('centerComponent');
									    centerPanel.removeAll(true);	
									    Ext.getCmp('centerComponent').setTitle(title);
										if (itemId == 'importProduct') {
											centerPanel.insert(0,importProductController.createHeader());
											centerPanel.insert(1,importProductController.createBody());
											centerPanel.insert(2,importProductController.createBottom());
										} else if (itemId == 'importRecord') {
											centerPanel.insert(0,importRecordController.createHeader());
											centerPanel.insert(1,importRecordController.createBody());
										} else if (itemId == 'exportProduct') {
											centerPanel.insert(0,exportProductController.createHeader());
											centerPanel.insert(1,exportProductController.createUpperBody());
											centerPanel.insert(2,exportProductController.createLowerBody());
											centerPanel.insert(3,exportProductController.createBottom());
										} else if(itemId =='exportRecord'){
											centerPanel.insert(0,exportRecordController.createHeader());
											centerPanel.insert(1,exportRecordController.createBody());
											centerPanel.insert(2,exportRecordController.createBottom());
										} else if(itemId=='accountDetail'){
											centerPanel.insert(0,checkAccountControler.createHeader());
											centerPanel.insert(1,checkAccountControler.createBody());
										} else if(itemId == 'queryInventory'){
											centerPanel.insert(0,storageQueryController.createHeader());
											centerPanel.insert(1,storageQueryController.createBody());
										} else if(itemId == 'checkInOut'){
											centerPanel.insert(0,recordInOutQueryController.createHeader());
											centerPanel.insert(1,recordInOutQueryController.createBody());
										} else if(itemId == 'borrow'){
											centerPanel.insert(0,productBorrowController.createHeader());
											centerPanel.insert(1,productBorrowController.createBody());
											centerPanel.insert(2,productBorrowController.createBottom());
										}else if(itemId == 'return'){
											centerPanel.insert(0,productReturnController.createHeader());
											centerPanel.insert(1,productReturnController.createBody());
											centerPanel.insert(2,productReturnController.createBottom());
										}else if(itemId == 'sellBuy'){
											centerPanel.insert(0,productSellBuyController.createHeader());
											centerPanel.insert(1,productSellBuyController.createBody());
										    centerPanel.insert(2,productSellBuyController.createBottom());
										}else if(itemId == 'customerRelationshipRecord'){
											centerPanel.insert(0,customerRelationshipRecordQueryController.createHeader());
											centerPanel.insert(1,customerRelationshipRecordQueryController.createBody());
										}
										centerPanel.doLayout();
										}
									},
							root : {
								expanded : true,
								children : [
										{
											text : '入库管理 - Import Management',
											expanded : true,
											children : [{
														id : 'importProduct',
														text : '货物入库 - Import Product',
														name : 'hello',
														leaf : true,
													},
													{
														id : 'importRecord',
														text : '入库记录 - Import Record',
														leaf : true
													} ]
										},
										{
											text : '出库管理 - Export Management',
											expanded : true,
											children : [
													{
														id : 'exportProduct',
														text : '货物出库 - Export Product',
														leaf : true
													},
													{
														id : 'exportRecord',
														text : '出库记录 - Export Record',
														leaf : true
													} ]
										},
										{
											text : 'Check Account',
											expanded : true,
											children : [ {
												id : 'accountDetail',
												text : '盘点 - Account Detail',
												leaf : true
											} ]
										},
										{
											text : '查询 - Query',
											expanded : true,
											children : [
													{
														id : 'queryInventory',
														text : '库存查询 - Query Inventory',
														leaf : true
													},{
														id : 'checkInOut',
														text : '记录查询 - Check In/Out',
														leaf : true
													} ]
										},{
											text : '客户关系 - Customer Relationship',
											expanded : true,
											children : [
													{
														id : 'borrow',
														text : '借 - Borrow',
														leaf : true
													},
													{
														id : 'return',
														text : '还 - Return',
														leaf : true
													},
													{
														id : 'sellBuy',
														text : '买/卖 - Sell/Buy',
														leaf : true
													},{
														id : 'customerRelationshipRecord',
														text : '记录 - relationship record',
														leaf : true
													} ]
										} ]
							}
						}) ]
				},{
				region : 'center',
				layout : 'border',
				split : true,
				width : '75%',
				height : "100%",
				border : true,
				items : [
					Ext.create('Ext.Panel', {
						id : 'centerComponent',
						layout : 'vbox',
						title : 'Detail',
						items : [importProductController.createHeader(),importProductController.createBody(),importProductController.createBottom()  ]
					}) 
				]
			} ]
		});
});

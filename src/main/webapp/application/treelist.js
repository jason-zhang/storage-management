/*global Ext:false */

/*
 This file is part of Ext JS 4
 GNU General Public License Usage
 */
Ext.require([ '*' ]);

var productInStoreData = Ext.create('Ext.data.Store', {
	storeId : 'simpsonsStore',
	fields : [ 'customer', 'product', 'batch','storage', 'quantity', 'from','importTime' ],
	data : {
		'items' : [ {
			'customer' : 'Jason',
			"product" : "Apt3",
			"batch" : "3016",
			"storage":"A-03-02",
			'quantity' : '30',
			"from" : "Auckland",
			"importTime" : "3016"
		}, {
			'customer' : 'Jason',
			"product" : "Apt3",
			"batch" : "3016",
			"storage":"A-03-02",
			'quantity' : '30',
			"from" : "Auckland",
			"importTime" : "3016"
		}, {
			'customer' : 'Jason',
			"product" : "Apt3",
			"batch" : "3016",
			"storage":"A-03-02",
			'quantity' : '90',
			"from" : "Auckland",
			"importTime" : "3016"
		}, {
			'customer' : 'Jason',
			"product" : "Apt3",
			"batch" : "3016",
			"storage":"A-03-02",
			'quantity' : '60',
			"from" : "Auckland",
			"importTime" : "3016"
		} ]
	},
	proxy : {
		type : 'memory',
		reader : {
			type : 'json',
			root : 'items'
		}
	}
});

var productInStoreTable = Ext.create('Ext.grid.Panel', {
	store : productInStoreData,
	columns : [ {
		text : 'Customer Name',
		dataIndex : 'customer'
	}, {
		text : 'Product type',
		dataIndex : 'product',
		flex : 1
	}, {
		text : 'Batch',
		dataIndex : 'batch'
	},{
		text : 'Storage',
		dataIndex : 'storage'
	}, {
		text : 'Quantity',
		dataIndex : 'quantity',
		flex : 1
	}, {
		text : 'From',
		dataIndex : 'from'
	}, {
		text : 'Import Time',
		dataIndex : 'importTime'
	} ],
	height : 300,
	width : 800
});

Ext.onReady(function() {
	// create the Grid

	Ext.create('Ext.Viewport', {
		layout : {
			type : 'border',
			padding : 5
		},
		defaults : {
			split : true
		},
		items : [ {
			region : 'west',
			collapsible : true,
			title : 'Storage System',
			split : true,
			width : '25%',
			height : "100%",
			minWidth : 100,
			minHeight : 140,
			items : [ Ext.create('Ext.tree.Panel', {
				rootVisible : false,
				width : "100%",
				height : "100%",
				root : {
					expanded : true,
					children : [ {
						text : 'Import Management',
						expanded : true,
						children : [ {
							text : 'Import Product',
							leaf : true
						}, {
							text : 'Import records',
							leaf : true
						} ]
					}, {
						text : 'Export Management',
						expanded : true,
						children : [ {
							text : 'Export Product',
							leaf : true
						}, {
							text : 'Export records',
							leaf : true
						} ]
					}, {
						text : 'Check Account',
						expanded : true,
						children : [ {
							text : 'Account Detail',
							leaf : true
						} ]
					}, {
						text : 'Query',
						expanded : true,
						children : [ {
							text : 'Query Inventory',
							leaf : true
						}, {
							text : 'Check In/Out',
							leaf : true
						} ]
					}, {
						text : 'Customer Relationship',
						expanded : true,
						children : [ {
							text : 'Borrow',
							leaf : true
						}, {
							text : 'Return',
							leaf : true
						}, {
							text : 'Sell/Buy',
							leaf : true
						} ]
					} ]
				}
			}) ]
		}, {
			region : 'center',
			layout : 'border',
			border : true,
			items : [ {
				region : 'center',
				title : 'Detail',
				minHeight : 80,
				layout : 'vbox',

				items : [ {
					xtype : 'container',
					layout : 'hbox',
					items : [ {
						xtype : 'container',
						layout : 'vbox',
						items : [ {
							xtype : 'textfield',
							name : 'name',
							fieldLabel : 'Customer ID',
							allowBlank : false
						}, {
							xtype : 'textfield',
							name : 'email',
							fieldLabel : 'Storage',
							vtype : 'email'
						}, {
							xtype : 'combo',
							fieldLabel : 'From',
							store : [ 'a', 'b' ],
							queryMode : 'local',
							displayField : 'name',
							valueField : 'abbr',
						}, {
							xtype : 'container',
							layout : 'hbox',

							items : [ {
								xtype : 'label',
								text : 'Import date:    '
							}, {
								margin : '1,80,10,100',
								xtype : 'label',
								text : '2013-05-10'
							} ]

						} ]
					}, {
						xtype : 'container',
						layout : 'vbox',
						items : [ {
							xtype : 'textfield',
							name : 'name',
							fieldLabel : 'Product Type',
							allowBlank : false
						}, {
							xtype : 'container',
							layout : 'hbox',

							items : [ {
								xtype : 'textfield',
								name : 'name',
								fieldLabel : 'Quantity',
								allowBlank : false
							}, {
								margin : '0,10,0,25',
								xtype : 'label',
								text : 'Box'
							} ]

						}, {
							xtype : 'combo',
							fieldLabel : 'Transport',
							store : [ 'a', 'b' ],
							queryMode : 'local',
							displayField : 'q',
							valueField : 'abbr',
						} ]
					}, {
						xtype : 'container',
						layout : 'vbox',
						items : [ {
							xtype : 'textfield',
							name : 'name',
							fieldLabel : 'Batch No',
							allowBlank : false
						}, {
							xtype : 'textfield',
							name : 'email',
							fieldLabel : 'Operator',
							vtype : 'email'
						}, {
							xtype : 'textfield',
							name : 'name',
							fieldLabel : 'Plate No',
							allowBlank : false
						}, {
							xtype : 'container',
							layout : 'hbox',
							items : [ {
								xtype : 'button',
								text : 'Add:    '
							}, {
								xtype : 'button',
								text : 'Delete:    '
							} ]

						} ]
					} ]
				}, {
					xtype : 'container',
					items : [ productInStoreTable ]
				},{
					xtype : 'button',
					text : 'Submit'
				} ]
			} ]
		} ]
	});
});

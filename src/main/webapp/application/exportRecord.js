/*global Ext:false */

/*
 This file is part of Ext JS 4
 GNU General Public License Usage
 */
Ext.require([ '*' ]);
var products = null;
Ext.Ajax.request({
	   url: 'rest/universal/getContent?type=storageLocation',
	   success: function(response){
		   products =  response.responseText.substring(1, response.responseText.length - 1);
	   }
});

/** export store */
Ext.define('Storage.export.record.Store',{
	extend:'Ext.data.Store',
	storeId : 'exportStore',
	fields : [ 'customerId', 'product', 'batchNo', 'quantity', 'operator', 'exportTime' ],
	autoLoad: 'false',
	listeners : {
		datachanged : function(obj,option) {
			var totalCount = 0;
			for(var index  = 0; index<obj.getCount() ; index++ ){
				totalCount += obj.getAt(index).data.quantity;
			}
			Ext.getCmp("exportexportProductTotal").text = totalCount;
		}
	},
	proxy: {
	    type: 'ajax',
	    url : 'rest/export/get_records',
	    reader: {
	    	type: 'json',
	    	root:'exportRecord'
	    }
	}
});

/** export record data */
Ext.define('Storage.export.record.Table',{
	extend:'Ext.grid.Panel',
	store : new Storage.export.record.Store(),
	id:'exportRecordTablePanel',
	columns : [ {
		text : 'Customer Id',
		dataIndex : 'customerId'
	}, {
		text : 'Product Type',
		dataIndex : 'product',
		flex : 1
	}, {
		text : 'Batch No',
		dataIndex : 'batchNo'
	}, {
		text : 'Quantity',
		dataIndex : 'quantity',
		flex : 1
	}, {
		text : 'Operator',
		dataIndex : 'operator'
	}, {
		text : 'Export Time',
		dataIndex : 'exportTime',
		renderer: function(value) {
			var exportTime = new Date(value);
			return Ext.Date.format(exportTime,'Y-m-d');
		}
	} ],
	height : 300,
	width : 800
});

/* Define a controller to control the create of Import Controller */
function ExportRecordController() {
	
};

// create product export condition panel
ExportRecordController.prototype.createHeader = function() {
	var header = Ext.create('Ext.container.Container', {
		id : 'queryConditonOfExportRecord',
		layout : 'vbox',
		items : [ {
			xtype : 'container',
			layout : 'hbox',
			items : [ {
				id: 'customerId',
				xtype : 'textfield',
				name : 'name',
				fieldLabel : 'Customer ID',
				allowBlank : false
			} ]
		}, {
			xtype : 'container',
			layout : 'hbox',
			items : [ {
				id: 'exportDateFrom',
				xtype : 'datefield',
				anchor : '100%',
				fieldLabel : 'Export Date From',
				maxValue : new Date()
			}, {
				id : 'exportDateTo',
				xtype : 'datefield',
				anchor : '100%',
				fieldLabel : ' To',
				name : 'from_date',
				maxValue : new Date()
			} ]
		}, {
			xtype : 'container',
			layout : 'hbox',
			items : [ {
				xtype : 'button',
				text : 'Query',
				listeners : {
					click : function(b, e) {
						Ext.getCmp('exportRecordTablePanel').getStore().reload({
						    params: {'customerId': Ext.getCmp('customerId').getValue(),
						    		 'exportDateFrom':Ext.Date.format( Ext.getCmp('exportDateFrom').getValue(), 'Y-m-d'),
						    		 'exportDateTo':Ext.Date.format( Ext.getCmp('exportDateTo').getValue(), 'Y-m-d')}
						});
					}
				}
			}, {
				xtype : 'button',
				text : 'Save as Excel'
			} ]
		} ]
	});
	return header;
};

//create product import condition panel
ExportRecordController.prototype.createBody = function() {
	var exportRecordTable = new Storage.export.record.Table();
	var exportTable = Ext.create('Ext.container.Container', {
		id : 'exportRecordTable',
		items : [ exportRecordTable ]
	});
	exportRecordTable.getStore().load({params:{}});
	
	return exportTable;
};

//Create import product table
ExportRecordController.prototype.createBottom = function() {
	var exportBottomPanel = Ext.create('Ext.panel.Panel', {
		id : 'productExportTableFootBar',
		items : [{
			id : 'exportTotalNumber',
			xtype : 'label',
			margin:'0 10 0 300',
			text : 'total: '
		},{
			id : 'exportexportProductTotal',
			xtype : 'label',
			text : ''
		},{
			id : 'exportEmtpyLableForExport',
			xtype : 'label',
			margin:'0 10 0 400',
			text : ''
		} ]
	});
	return exportBottomPanel;
}


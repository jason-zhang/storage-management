/*global Ext:false */

/*
 This file is part of Ext JS 4
 GNU General Public License Usage
 */
Ext.require([ '*' ]);

/**
 * storage in out query record
 */
Ext.define('Storage.record.customerRelationship.Store', {
	extend : 'Ext.data.Store',
	storeId : 'customerRelationshipStore',
	fields : [ 'productFrom', 'productTo', 'product', 'batchNo', 'quantity', 'actionTime', 'actionType' ],
	proxy: {
	    type: 'ajax',
	    url : 'rest/customer_relationship/get_records',
	    reader: {
	    	type: 'json',
	    	root:'customer_relationship'
	    }
	}
});

/**
 * Storage query panel
 */
Ext.define('Storage.customer.relationship.record.Table', {
	extend : 'Ext.grid.Panel',
	id : 'recordCustomerRelationshipRecordQuery',
	store : new Storage.record.customerRelationship.Store(),
	height : 400,
	width : 950,
	columns : [{
		text : 'Action Type',
		dataIndex : 'actionType',
		flex : 1
	}, {
		text : 'Product Source From',
		dataIndex : 'productFrom',
		flex : 1
	},{
		text : 'Product Flow To',
		dataIndex : 'productTo',
		flex : 1
	}, {
		text : 'Product Type',
		dataIndex : 'product',
		flex : 1
	}, {
		text : 'Batch No',
		dataIndex : 'batchNo',
		flex : 1
	}, {
		text : 'Quantity',
		dataIndex : 'quantity',
		flex : 1
	}, {
		text : 'Action Time',
		dataIndex : 'actionTime',
		flex : 1
	}]
});

/**
 * 
 */
function CustomerRelationshipRecordQueryController() {
	
}

/**
 * 
 */
CustomerRelationshipRecordQueryController.prototype.createHeader = function() {
	var customerRelationshipContainer = Ext.create('Ext.container.Container',{
				id : 'customerRelationshipContainer',
				layout : 'vbox',
				items : [ {
					xtype : 'container',
					layout : 'hbox',
					items : [ {
						id : "productFromForCustomerRelationshipRecord",
						xtype : 'textfield',
						name : 'name',
						fieldLabel : 'Product From'
					}, {
						id : "productToForCustomerRelationshipRecord",
						xtype : 'textfield',
						name : 'name',
						fieldLabel : 'Product To'
					}, {
						id:'productForCustomerRelationshipRecord',
						xtype : 'combo',
						fieldLabel : 'Product Type',
						store : [ 'apt1', 'apt2' ]
					} ]
				}, {
					xtype : 'container',
					layout : 'hbox',
					items : [ {
						id: 'operationDateFromForCustomerRelationshipRecord',
						xtype : 'datefield',
						anchor : '100%',
						fieldLabel : 'Date From',
						maxValue : new Date()
					}, {
						id: 'operationDateToForCustomerRelationshipRecord',
						xtype : 'datefield',
						anchor : '100%',
						fieldLabel : 'To',
						maxValue : new Date()
					},{
						xtype : 'container',
						layout : 'hbox',
						items : [ {
							xtype : 'button',
							text : 'Go',
							listeners : {
								click : function(b, e) {
									Ext.getCmp('recordCustomerRelationshipRecordQuery').getStore().reload({
									    params: {'productFrom': Ext.getCmp('productFromForCustomerRelationshipRecord').getValue(),
									    	'productTo': Ext.getCmp('productToForCustomerRelationshipRecord').getValue(),
									    	'product': Ext.getCmp('productForCustomerRelationshipRecord').getValue(),  
									    	"dateFrom" : Ext.Date.format(Ext.getCmp('operationDateFromForCustomerRelationshipRecord').getValue(),'Y-m-d'),
									    	"dateTo" : Ext.Date.format(Ext.getCmp('operationDateToForCustomerRelationshipRecord').getValue(),'Y-m-d')}
									} );
								}
							}
						}, {
							xtype : 'button',
							text : 'Save as Excel'
						} ]
					} ]
				} ]
			});
	return customerRelationshipContainer;
}

/**
 * 
 */
CustomerRelationshipRecordQueryController.prototype.createBody = function() {
	var customerRelationshipTable = new Storage.customer.relationship.record.Table();
	var queryForCustomerRelationshipRecord = Ext.create('Ext.container.Container', {
		id : 'queryForCustomerRelationshipRecord',
		items : [ customerRelationshipTable ]
	});
	customerRelationshipTable.getStore().load({params:{}});
	return queryForCustomerRelationshipRecord;
}

package org.storagemanagement;

import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.storagemanagement.DAO.ProductImportDao;
import org.storagemanagement.DO.ProductImportDO;
import org.storagemanagement.model.ImportRecordQuery;

/**
 * Unit test for simple App.
 */
public class ImportProductDAOTest {
	/**
	 * Rigourous Test :-)
	 */
	// @Test
	public void testQuery() {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("BeanLocations.xml");
		
		ProductImportDao productAddDao = (ProductImportDao) appContext.getBean("productImportDao");
		ImportRecordQuery importQuery = new ImportRecordQuery();
		importQuery.setCustomerId("Jason");
		importQuery.setProduct("apt1");
		importQuery.setBatchNo("3016");
		importQuery.setImportDateFrom("2013-09-29");
		importQuery.setImportDateTo("2013-09-30");
		List<ProductImportDO> importDOs = productAddDao.find(importQuery);
		Assert.assertEquals(importDOs.size(), 1);
	}
	/**
	 * Rigourous Test :-)
	 */
	// @Test
	public void testupdate() {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("BeanLocations.xml");
		
		ProductImportDao productAddDao = (ProductImportDao) appContext.getBean("productAddDao");
		ProductImportDO productAddRecordDO = new ProductImportDO();
		productAddRecordDO.setId(35);
		productAddRecordDO.setCustomerId("jason222");
		productAddRecordDO.setProduct("apt444");
		productAddRecordDO.setBatchNo("4444");
		productAddDao.update(productAddRecordDO);
	}
	
	//@Test
	public void testDelete() {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("BeanLocations.xml");
		
		ProductImportDao productAddDao = (ProductImportDao) appContext.getBean("productAddDao");
		ProductImportDO productAddRecordDO = new ProductImportDO();
		productAddRecordDO.setId(35);
		productAddDao.delete(productAddRecordDO);
	}
	
	//@Test
	public void testSave() {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("BeanLocations.xml");
		
		ProductImportDao productAddDao = (ProductImportDao) appContext.getBean("productAddDao");
		ProductImportDO productAddRecordDO = new ProductImportDO();
		productAddRecordDO.setId(4);
		productAddRecordDO.setCustomerId("Jason");
		productAddRecordDO.setProduct("apt1");
		productAddRecordDO.setBatchNo("12345");
		productAddRecordDO.setImportFrom("Auckland");
		productAddRecordDO.setStorage("A-03");
		productAddRecordDO.setTransport("A company");
		productAddRecordDO.setImportTime(new Date());
		productAddRecordDO.setPlateNo("BNL157");
		productAddRecordDO.setGmtCreated(new Date());
		productAddRecordDO.setOperator("Violette");
		productAddRecordDO.setQuantity(20);
		
		productAddDao.save(productAddRecordDO);
	}
}

package org.storagemanagement;

import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.storagemanagement.DAO.ProductExportDao;
import org.storagemanagement.DO.ProductExportDO;
import org.storagemanagement.model.ExportRecordQuery;

/**
 * Unit test for simple App.
 */
public class ExportProductDAOTest {

	private ProductExportDao productExportDao;

	@Before
	public void init() {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("BeanLocations.xml");
		this.productExportDao = (ProductExportDao) appContext.getBean("productExportDao");
	}

	// @Test
	public void testQuery() {

		ExportRecordQuery exportQuery = new ExportRecordQuery();
		exportQuery.setCustomerId("Leon");
		exportQuery.setExportDateFrom("2013-09-26");
		exportQuery.setExportDateTo("2013-09-27");
		List<ProductExportDO> exportDOs = productExportDao.find(exportQuery);
		Assert.assertEquals(exportDOs.size(), 7);
	}

	// @Test
	public void testUpdate() {

		ProductExportDO productReduceRecordDO = new ProductExportDO();
		productReduceRecordDO.setId(5);
		productReduceRecordDO.setBatchNo("30161");
		productReduceRecordDO.setBatchNo("99991");
		productReduceRecordDO.setOperator("Jason1");
		productReduceRecordDO.setProduct("apt11");
		productReduceRecordDO.setQuantity(31);
		productReduceRecordDO.setCustomerId("Leon1");
		productReduceRecordDO.setGmtCreated(new Date());

		productExportDao.update(productReduceRecordDO);
	}

	// @Test
	public void testDelete() {
		ProductExportDO productReduceRecordDO = new ProductExportDO();
		productReduceRecordDO.setId(13);

		productExportDao.delete(productReduceRecordDO);
	}

	// @Test
	public void testSave() {
		ProductExportDO productReduceRecordDO = new ProductExportDO();
		productReduceRecordDO.setBatchNo("3016");
		productReduceRecordDO.setOperator("Jason");
		productReduceRecordDO.setProduct("apt1");
		productReduceRecordDO.setQuantity(30);
		productReduceRecordDO.setCustomerId("Leon1");
		productReduceRecordDO.setGmtCreated(new Date());
		productReduceRecordDO.setExportTime(new Date());

		productExportDao.save(productReduceRecordDO);
	}
}

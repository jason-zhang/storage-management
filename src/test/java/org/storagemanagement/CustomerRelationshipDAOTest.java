package org.storagemanagement;

import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.storagemanagement.DAO.CustomerRelationshipDao;
import org.storagemanagement.DAO.ProductImportDao;
import org.storagemanagement.DO.CustomerRelationshipDO;
import org.storagemanagement.DO.ProductImportDO;
import org.storagemanagement.model.CustomerRelationshipQuery;
import org.storagemanagement.uti.OperationTypeEnum;

/**
 * Unit test for simple App.
 */
public class CustomerRelationshipDAOTest {
	private CustomerRelationshipDao customerRelationshipDao;
	
	@Before
	public void init(){
		ApplicationContext appContext = new ClassPathXmlApplicationContext("BeanLocations.xml");
		
		this.customerRelationshipDao = (CustomerRelationshipDao) appContext.getBean("customerRelationshipDao");
	}

	//@Test
	public void testQuery() {
		
		CustomerRelationshipQuery query = new CustomerRelationshipQuery();
		query.setProductFrom("jason3");
		query.setProductTo("leo2");
		query.setProduct("apt11");
		query.setDateFrom("2013-09-29");
		query.setDateTo("2013-09-30");
		List<CustomerRelationshipDO> importDOs = this.customerRelationshipDao.find(query);
		Assert.assertEquals(importDOs.size(), 2);
	}

	//@Test
	public void testAdd() {
		
		CustomerRelationshipDO customerRelationshipDO = new CustomerRelationshipDO();
		customerRelationshipDO.setId(35);
		customerRelationshipDO.setProductFrom("jason");
		customerRelationshipDO.setProductTo("leo");
		customerRelationshipDO.setProduct("apt11");
		customerRelationshipDO.setBatchNo("4444");
		customerRelationshipDO.setOperationType(OperationTypeEnum.BORROW.getType());
		customerRelationshipDO.setOperator("Sarah");
		customerRelationshipDO.setQuantity(20);
		customerRelationshipDO.setActionDate(new Date());
		customerRelationshipDO.setGmtCreate(new Date());
		
		customerRelationshipDao.add(customerRelationshipDO);
	}
	
	//@Test
	public void testDelete() {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("BeanLocations.xml");
		
		ProductImportDao productAddDao = (ProductImportDao) appContext.getBean("productAddDao");
		ProductImportDO productAddRecordDO = new ProductImportDO();
		productAddRecordDO.setId(35);
		productAddDao.delete(productAddRecordDO);
	}
	
	//@Test
	public void testSave() {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("BeanLocations.xml");
		
		ProductImportDao productAddDao = (ProductImportDao) appContext.getBean("productAddDao");
		ProductImportDO productAddRecordDO = new ProductImportDO();
		productAddRecordDO.setId(4);
		productAddRecordDO.setCustomerId("Jason");
		productAddRecordDO.setProduct("apt1");
		productAddRecordDO.setBatchNo("12345");
		productAddRecordDO.setImportFrom("Auckland");
		productAddRecordDO.setStorage("A-03");
		productAddRecordDO.setTransport("A company");
		productAddRecordDO.setImportTime(new Date());
		productAddRecordDO.setPlateNo("BNL157");
		productAddRecordDO.setGmtCreated(new Date());
		productAddRecordDO.setOperator("Violette");
		productAddRecordDO.setQuantity(20);
		
		productAddDao.save(productAddRecordDO);
	}
}

/**
 * 
 */
package org.storagemanagement;

import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.storagemanagement.DAO.UniversalDao;
import org.storagemanagement.DO.UniversalDO;

/**
 * @author jason.zhang
 *
 */
public class UniversalDAOTest {

	private UniversalDao universalDao;

	@Before
	public void init() {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("BeanLocations.xml");
		this.universalDao = (UniversalDao) appContext.getBean("universalDao");
	}

	//@Test
	public void testQuery() {

		List<UniversalDO> exportDOs = universalDao.findByType("product");
		Assert.assertEquals(exportDOs.size(), 2);
	}
}